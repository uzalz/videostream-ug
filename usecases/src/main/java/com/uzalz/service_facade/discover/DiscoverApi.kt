package com.uzalz.service_facade.discover

import com.uzalz.service_facade.entities.Channel
import com.uzalz.service_facade.entities.Subscription
import com.uzalz.service_facade.entities.Video
import io.reactivex.Observable
import io.reactivex.Observer


import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.*


interface DiscoverApi {


    @GET("getchannels/")
    fun getChannels() : Observable<MutableList<Channel>>

    @GET("getchannel/{channelId}")
    fun getChannel(@Path("channelId") channelId:String) : Observable<Channel>

    @GET("getchannelvideos/{channelId}")
    fun getChannelVideos(@Path("channelId") channelId:String) : Observable<MutableList<Video>>

    @GET("getvideos/")
    fun getVideos() : Observable<MutableList<Video>>

    @POST("addsubscription/{channelId}")
    fun subscribe(@Path("channelId") channelId:String, @Body subscription: Subscription): Observable<Subscription>




    //what I remember here the request is normally an http request
    //but the controller gets the request and converts it into an object request the use case understands


}
package com.uzalz.service_facade.discover

import com.uzalz.service_facade.entities.Channel
import com.uzalz.service_facade.entities.Subscription
import com.uzalz.service_facade.entities.Video
import io.reactivex.Observable

class DiscoverRepository(var discoverApi: DiscoverApi) {

    var mCachedVideos: LinkedHashMap<String, Video> ? = null
    var isCacheDirty = false // means the data is old



    fun getVideos(): Observable<MutableList<Video>> {

          return discoverApi.getVideos()

    }

    fun getChannels(): Observable<MutableList<Channel>> {

        return discoverApi.getChannels()

    }

    fun subscribeToChannel(channelId: String, subscription: Subscription):Observable<Subscription>{

       return discoverApi.subscribe(channelId,subscription)

    }


    /*
    fun getVideos(){
        // Respond immediately with cache if available and not dirty
        if(mCachedVideos?.isNotEmpty()!! && !isCacheDirty){

           var vids =  ArrayList<Video>(mCachedVideos!!.values)
            return
        }

        if(isCacheDirty){ //we need new data on internet connection
            // If the cache is dirty we need to fetch new data from the network.

        } else{
            // Query the local storage if available. If not, query the network.
            // updateCache()

        }

    }
     */



    fun updateCache(videos: List<Video>){
        if (mCachedVideos== null) {
            mCachedVideos = LinkedHashMap<String,Video>();
        }
        mCachedVideos?.clear()

        for (video in videos) {
            mCachedVideos?.put(video.videoId, video);
        }
        isCacheDirty = false;

    }



    fun refresh(){
        //make cache dirty when you know there is internet conn
       isCacheDirty = true

    }





}
package com.uzalz.service_facade.discover

import com.uzalz.service_facade.UseCase
import com.uzalz.service_facade.entities.Video
import java.util.*

class DiscoverVideo(var discoverRepository: DiscoverRepository):UseCase<DiscoverVideo.RequestValue,DiscoverVideo.ResponseValue> {




    override fun execute(request: RequestValue, delivery: UseCase.OutputBoundary<ResponseValue>) {


        if(request.featureType == NEW){

             val vids = discoverRepository.getVideos()
          //  delivery.onSuccess(ResponseValue(vids))

        }
        else if(request.featureType == TRENDING){

            val vids = discoverRepository.getVideos()
        //    delivery.onSuccess(ResponseValue(vids))

        }



    }

    data class RequestValue(val featureType: Int):UseCase.RequestValue

    data class ResponseValue(val videos: List<Video>):UseCase.ResponseValue


    companion object{
        lateinit  var  responseValue : ResponseValue
        val NEW = 1
        val TRENDING  = 2
        val CATEGORY  = 3
        val LATER  = 4
        val DOWNLOADS = 5
        val PURCHASES = 6
    }

}
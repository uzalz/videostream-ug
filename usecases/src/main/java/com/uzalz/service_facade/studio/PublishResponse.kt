package com.uzalz.service_facade.studio

import java.util.*

class PublishResponse {

    var title : String ? = null
    var description : String ? = null
    var runtime : String ? = null

    var contentId : String ? = null
    var videoUrl : String ? = null


}
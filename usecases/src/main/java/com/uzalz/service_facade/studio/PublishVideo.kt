package com.uzalz.service_facade.studio
import android.util.Log
import android.view.View
import com.uzalz.service_facade.UseCase
import com.uzalz.service_facade.entities.Video
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import okhttp3.ResponseBody
import java.util.*

class PublishVideo(private var studioRepo: StudioRepository) : UseCase<PublishVideo.RequestValue, PublishVideo.ResponseValue> { // define the type


    override fun execute(request: RequestValue, delivery: UseCase.OutputBoundary<ResponseValue>) {

        //1. get the request value
        //2. shape it into  streaming video details
             val publishVideoRequest = PublishRequest()
             publishVideoRequest.title = request.title
             publishVideoRequest.description = request.description
             publishVideoRequest.videoPath = request.videoPath



        //3. publish the video to the streaming host //use repository layer
             studioRepo.publishVideoForStreaming(publishVideoRequest)
                 .subscribeOn(Schedulers.io())
                 .subscribe(object : DisposableObserver<PublishResponse>() {//PublishResponse

                     override fun onNext(t: PublishResponse) {

                        // Log.d("PublishVideo", t.string())

                         //4. then get the profile and publish it and get its url
                        // val posterUrl = studioRepo.uploadMedia()

                         //5. then get the details and shape them into the video details

                         val video = Video()
                         video.title = request.title
                         video.category = request.category
                         video.description = request.description
                         video.crew = request.crew
                         video.director = request.director
                         video.language = request.language
                         video.releaseDate = request.releaseDate
                         video.price = request.price

                         video.year = request.year
                         video.pgRating = request.pgRating
                         video.starRating = "0.0"

                         //This is got after publishing video
                         video.runtime = t.runtime
                         video.contentId = t.contentId
                         video.previewContentId = ""
                         video.videoUrl = t.videoUrl



                         //posterUrl is filled on return of video details
                         video.posterUrl = request.posterPath



                         //starRating is filled after time but initially zero

                         Log.d("PublishActivityD", "Video_title: "+request.title)

                         //6. then publish them to the firebase server
                         executeUploadVideoDetails(request.channelId,video,delivery)


                     }

                     override fun onError(e: Throwable) {

                     }

                     override fun onComplete() {


                     }

                 })

    }


    private fun executeUploadVideoDetails(channelId: String, video: Video, delivery: UseCase.OutputBoundary<ResponseValue>){
        Log.d("PublishActivityD", "execute: "+channelId)

        studioRepo.uploadVideoDetails(channelId,video)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object: DisposableObserver<Video>() {

                override fun onNext(t: Video) {


                    delivery.onSuccess(ResponseValue(t))

                }

                override fun onComplete() {

                }

                override fun onError(e: Throwable) {

                    delivery.onNetworkFail(e.message!!)

                }


            })

    }


    data class RequestValue(val channelId:String,val title:String, val description:String,val director:String,val crew:String,
                            val releaseDate: Date, val language:String, val category:String,
                            val price:String, val year: Int, val pgRating:String,
                            val videoPath:String,val posterPath:String) : UseCase.RequestValue



    data class ResponseValue(val video: Video?):UseCase.ResponseValue



}
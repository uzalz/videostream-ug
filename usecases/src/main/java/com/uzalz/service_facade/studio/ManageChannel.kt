package com.uzalz.service_facade.studio
import com.uzalz.service_facade.UseCase
import com.uzalz.service_facade.entities.Channel
import com.uzalz.service_facade.entities.Video
import java.util.*

class ManageChannel(var studioRepo: StudioRepository) : UseCase<ManageChannel.RequestValue, ManageChannel.ResponseValue> { // define the type


    override fun execute(request: RequestValue, delivery: UseCase.OutputBoundary<ResponseValue>) {



    }


    fun updateChannel(channel:String){


    }

    fun createChannel(){


    }



    data class RequestValue(val channelName: String, val channelDescription: String, val channelAvator: String):UseCase.RequestValue


    data class ResponseValue(val channel: Channel?):UseCase.ResponseValue



}
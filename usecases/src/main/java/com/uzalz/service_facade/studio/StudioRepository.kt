package com.uzalz.service_facade.studio

import com.android.internal.http.multipart.MultipartEntity
import com.uzalz.service_facade.entities.Channel
import com.uzalz.service_facade.entities.Video
import io.reactivex.Observable
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.apache.hc.client5.http.classic.methods.HttpPost
import org.apache.hc.client5.http.classic.methods.HttpUriRequest
import org.apache.hc.client5.http.entity.mime.FileBody
import org.apache.http.HttpResponse
import org.apache.http.client.methods.HttpPut
import org.apache.http.impl.client.DefaultHttpClient
import java.io.File
import java.io.IOException
import java.text.ParseException


class StudioRepository(val studioApi: StudioApi) {


    private var myChannels :  MutableList<Channel> ? = null

    init {


    }

    fun createChannel(channel: Channel):Observable<Channel>{
    //   myChannels?.add(channel)

        //API
       return studioApi.createChannel(channel)

      //return  Observable.just(channel)

    }


    fun getChannels(): Observable<MutableList<Channel>> {

        return studioApi.getChannels()

      //  return Observable.just(myChannels)
    }

    fun getChannel(channelId: String): Observable<Channel> {

        return studioApi.getChannel(channelId)

        //  return Observable.just(myChannels)
    }


    fun getChannelVideos(): Observable<MutableList<Channel>> {

        return studioApi.getChannels()

        //  return Observable.just(myChannels)
    }


    fun editChannel(channel_id: String, channelDesc:String):Observable<Boolean>{


        return Observable.just(true)

    }


    fun uploadVideoDetails(channelId: String,video: Video):Observable<Video>{
        //access firebase api and upload video details

      return  studioApi.uploadVideoDetails(channelId,video)
    }


    //PublishResponse
    fun publishVideoForStreaming(publishRequest:PublishRequest):Observable<PublishResponse>{

        val videoFile = File(publishRequest.videoPath!!)
        val videoBody: RequestBody = RequestBody.create(MediaType.parse("video/*"), videoFile)

        val vFile = MultipartBody.Part.createFormData("video", videoFile.name, videoBody)

        val res = PublishResponse()
        res.runtime ="2h-13m"
        res.description ="video Description"
        res.title ="video Title"
        res.videoUrl = vFile.toString()

       //return studioApi.publishVideo("my video",vFile)

        return Observable.just(res)
    }


    @Throws(ParseException::class, IOException::class)



    fun editVideo(channel_id: String, video: Video):Boolean{

        /*
        //First, find the position of the video in the list
        val videoPosition= myChannels.indexOfFirst {
            it.channelId == channel_id
        }

        //Now get your video by position and make changes
        val updatedVideo = myChannels[videoPosition].apply {
            //Make all changes you need here
            channelDescription = channelDesc
        }

        //Finally, replace updated video into your list.
        myChannels[videoPosition] = updatedVideo

         */

        return true

    }





}
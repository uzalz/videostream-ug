package com.uzalz.service_facade.studio


import com.uzalz.service_facade.entities.Channel
import com.uzalz.service_facade.entities.Video
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*


interface StudioApi {


    @GET("getchannels/")
    fun getChannels() : Observable<MutableList<Channel>>

    @GET("getchannel/{channelId}")
    fun getChannel(@Path("channelId") channelId:String) : Observable<Channel>


    @POST("addvideochannel/")
    fun createChannel(@Body channel: Channel): Observable<Channel>


    @GET("getchannelvideos/{channelId}")
    fun getChannelVideos(@Path("channelId") channelId:String) : Observable<MutableList<Video>>

    @GET("getvideos/")
    fun getVideos() : Observable<MutableList<Video>>

    @POST("addvideotochannel/{channelId}")
    fun uploadVideoDetails(@Path("channelId") channelId:String, @Body video: Video): Observable<Video>


    //what I remember here the request is normally an http request
    //but the controller gets the request and converts it into an object request the use case understands


    @Multipart
    @POST("publishvideo/")
    fun publishVideo(@Part("description") description: String?, @Part("video") video: MultipartBody.Part): Observable<ResponseBody>





}
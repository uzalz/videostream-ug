package com.uzalz.service_facade.entities

class Review {

    var title :String ? = null
    var subtitle :String ? = null
    var reviewer : String ? = null
    var rating : String ? = null
    var date : String ? = null

}
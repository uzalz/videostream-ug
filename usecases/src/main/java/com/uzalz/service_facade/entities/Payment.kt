package com.uzalz.service_facade.entities

import java.util.*

class Payment {

    var payId : String ? = null
    var payAmount : String ? = null
    var payDate = Date()
    var userId : String ? = null
    var videoId : String ? = null

}
package com.uzalz.service_facade.entities

import android.os.Parcel
import android.os.Parcelable
import java.util.*

class Video() : Parcelable {

    var videoId = UUID.randomUUID().toString()

    init {
    }

    var contentId : String ? = null
    var previewContentId : String ? = null

    var posterUrl : String ? = null
    var videoUrl : String ? = null

    var title : String ? = null
    var description : String ? = null
    var releaseDate : Date ? = null
    var language : String ? = null
    var category : String ? = null
    var year : Int ? = null
    var price : String ? = null

    var pgRating: String ? = null
    var starRating : String ? = null

    var runtime : String ? = null
    var director : String ? = null
    var crew: String ? = null

    ////////////////////////////////////////////////////////////
    var views : List<Watch> ? = null

    constructor(parcel: Parcel) : this() {
        videoId = parcel.readString().toString()
        contentId = parcel.readString()
        previewContentId = parcel.readString()
        posterUrl = parcel.readString()
        videoUrl = parcel.readString()
        title = parcel.readString()
        description = parcel.readString()
        language = parcel.readString()
        category = parcel.readString()
        year = parcel.readValue(Int::class.java.classLoader) as? Int
        price = parcel.readString()
        pgRating = parcel.readString()
        starRating = parcel.readString()
        runtime = parcel.readString()
        director = parcel.readString()
        crew = parcel.readString()
    }


    /////////////////////////   DESIGN METHODS   //////////////

    fun getNoViews():Int{

         return views!!.size
    }

    fun calculateRating(totalRating : Int):String{
        // convert the rating into a value between 10 or other factors

        return "rating"
    }


    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(videoId)
        dest?.writeString(contentId)
        dest?.writeString(previewContentId)
        dest?.writeString(posterUrl)
        dest?.writeString(videoUrl)
        dest?.writeString(title)
        dest?.writeString(description)
        dest?.writeString(language)
        dest?.writeString(category)
        dest?.writeValue(year)
        dest?.writeString(price)
        dest?.writeString(pgRating)
        dest?.writeString(starRating)
        dest?.writeString(runtime)
        dest?.writeString(director)
        dest?.writeString(crew)
    }

    override fun describeContents(): Int {
        return 0

    }

    companion object CREATOR : Parcelable.Creator<Video> {
        override fun createFromParcel(parcel: Parcel): Video {
            return Video(parcel)
        }

        override fun newArray(size: Int): Array<Video?> {
            return arrayOfNulls(size)
        }
    }


}
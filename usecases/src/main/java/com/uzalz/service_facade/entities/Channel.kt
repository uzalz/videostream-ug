package com.uzalz.service_facade.entities

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*
import kotlin.collections.ArrayList

@Parcelize
class Channel : Parcelable {

    var channelId : String ? = null
    var channelName : String ? = null
    var channelDescription : String ? = null
    var channelAvatorUrl : String ? = null


    ////////////////////////////////////////
    var videos :  MutableList<Video> ? = null
    var subscriptions : MutableList<Subscription> ? = null

    init {
        videos  = ArrayList()
        videos = ArrayList()
        subscriptions = ArrayList()
        channelId = UUID.randomUUID().toString()
    }


    ///////////////////////////////////////
    var subs = 0
    fun getNoSubscriptions():Int{

        if(subscriptions!!.isNotEmpty() || subscriptions != null){
          subs =   subscriptions!!.size

        }
        return subs

    }

    fun isProfileComplete():Boolean{

        return channelName != null && channelDescription != null
    }

}
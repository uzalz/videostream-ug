package com.uzalz.service_facade

interface UseCase <P : UseCase.RequestValue,R: UseCase.ResponseValue> {
    //im locking the only var to be used for my methods to be of that type

    interface OutputBoundary<R>{  // R is the output boundary that receives the data
                                  // this interface is implemented by it

        fun onSuccess(result: R)
        fun onNetworkFail(e:String)
        fun onInvalidMessage(e:String)
    }

    fun execute(request : P, delivery : OutputBoundary<R>)  //i get the request process it send it to the out


    interface RequestValue
    interface ResponseValue

}
package com.uz.streamuganda


import android.content.Context
import android.graphics.Typeface
import android.os.Bundle
import android.text.*
import android.text.style.StyleSpan
import android.util.Log
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.isEmpty
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.mancj.materialsearchbar.MaterialSearchBar
import com.mancj.materialsearchbar.SimpleOnSearchActionListener
import com.uz.streamuganda.discover.*
import com.uz.streamuganda.utilities.RecyclerManager
import com.uzalz.service_facade.entities.Video
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(),BottomNavigationView.OnNavigationItemSelectedListener,VideoAdapter.Videolistner  {


    private lateinit var suggestions : ArrayList<Video>
    private lateinit var customSuggestionsAdapter : CustomSuggestionsAdapter
    private lateinit var searchAdapter : VideoAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
       // setUpToolBar()

        setUpBottomNavigation()
        loadHome()
        setUpSearchBar()
        //setUpRecyclerView()

        /*
        val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        //searchAdapter = CustomSuggestionsAdapter(inflater)
        customSuggestionsAdapter = CustomSuggestionsAdapter(inflater,this)
        suggestions = getSomeVideos()
        customSuggestionsAdapter.suggestions = suggestions
        searchBar.setCustomSuggestionAdapter(customSuggestionsAdapter)
         */


        //searchAdapter = VideoAdapter(this,getSomeVideos(),this)
        //searchBar.setCustomSuggestionAdapter(searchAdapter)
        //updateContent(getSomeVideos())

    }



    private fun getSomeVideos():ArrayList<Video>{

        suggestions = ArrayList()
        var vid1 = Video()
        vid1.title = "Kumpinawe"
        vid1.price = "20,000"

        var vid2 = Video()
        vid2.title = "Yani"
        vid2.price = "30,000"

        suggestions.add(vid1)
        suggestions.add(vid2)


        return suggestions
    }

    private fun setUpToolBar() {

        var toolbar = findViewById<Toolbar>(R.id.toolbar_main)

        //    toolbar_main.logo = resources.getDrawable(R.drawable.ic_logo)
        // toolbar_main.navigationIcon = resources.getDrawable(R.drawable.ic_arrow_back_black_24dp)
        setSupportActionBar(toolbar)
        //supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        //  supportActionBar!!.setIcon(R.drawable.ic_logo)
        supportActionBar!!.setDisplayShowTitleEnabled(true)



        val str = SpannableStringBuilder("VS-UG")
        str.setSpan(StyleSpan(Typeface.BOLD), 0, 4, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        toolbar.title = str
        toolbar.titleMarginStart = resources.getInteger(R.integer.toolbar_title_margin)
        supportActionBar!!.title = str




        //toolbar.setTitle("VimsApp");
//toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimary));
//((AppCompatActivity)getActivity()).getSupportActionBar().setLogo(R.drawable.notice_48);
        val collapsingToolbarLayout: CollapsingToolbarLayout = findViewById(R.id.collapsing_toolbar)

        ///collapsingToolbarLayout.setExpandedTitleGravity(Gravity.BOTTOM);
        ///collapsingToolbarLayout.setExpandedTitleGravity(Gravity.BOTTOM);

       // collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar)
       // collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar)
        collapsingToolbarLayout.setCollapsedTitleGravity(Gravity.START)
        collapsingToolbarLayout.setTitleEnabled(false)
        collapsingToolbarLayout.setTitle("VimsApp")



    }



    //-------------------SEARCH BAR  -------------------------------------------------------/

    private fun setUpSearchBar(){
        // Search bar
        var searchBar = searchBar
        //enable searchbar callbacks
        //searchBar.setOnSearchActionListener(this);
        //restore last queries from disk
        //lastSearches = searchBar.getLastSuggestions();
        //searchBar.setLastSuggestions(lastSearches);
        //Inflate menu and setup OnMenuItemClickListener
        searchBar.inflateMenu(R.menu.main);
        //searchBar.setSearchIcon(R.drawable.ic_search);
        //searchBar.setRoundedSearchBarEnabled(true)
        //searchBar.toolbar_main.title = "TubeJet"
        //searchBar.setBackgroundColor(resources.getColor(R.color.windowBackground));
        searchBar.setCardViewElevation(20);
        //searchBar.setDividerColor(R.color.colorAccent);
        //searchBar.setTextColor(R.color.colorWhite)
        //searchBar.setPlaceHolderColor(getResources().getColor(R.color.light_blue));
        searchBar.setNavButtonEnabled(false);
        // SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
       // var cn = ComponentName(this, SearchCaretakerActivity.class);
       // searchBar.enableSearch()
        searchBar.showSuggestionsList()

        searchBar.setOnSearchActionListener(object: MaterialSearchBar.OnSearchActionListener {
            override fun onButtonClicked(buttonCode: Int) {

                when (buttonCode) {
                    MaterialSearchBar.BUTTON_BACK -> searchCard.visibility = View.GONE
                    MaterialSearchBar.BUTTON_SPEECH -> searchCard.visibility = View.GONE
                }
            }

            override fun onSearchConfirmed(text: CharSequence?) {

            }

            override fun onSearchStateChanged(enabled: Boolean) {

            }


        })



        searchBar.addTextChangeListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                Log.d("LOG_MAIN_ACTIVITY",  " text changed " + searchBar.getText())
               // searchAdapter.getFilter().filter(searchBar.text);
                customSuggestionsAdapter.filter.filter(searchBar.text);
               // mylist.visibility=View.VISIBLE
               // searchCard.visibility = View.VISIBLE
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (TextUtils.isEmpty(s)){
                        //Text is cleared, do your thing
                        searchCard.visibility = View.GONE
                        Log.d("LOG_MAIN_ACTIVITY", " text changed empty now ")
                }

            }


        })


    }




    private fun setUpRecyclerView(){
      //  mylist.visibility=View.GONE

        val staggeredGridLayoutManager =
            StaggeredGridLayoutManager(3, LinearLayoutManager.VERTICAL)

        // val layoutManager = LinearLayoutManager(context)
        var gridColumns = 3
        val layoutManager =  GridLayoutManager(this, gridColumns, GridLayoutManager.VERTICAL, false)

        mylist.layoutManager = staggeredGridLayoutManager
        RecyclerManager.decorateRecyclerView(mylist,this)
        mylist.layoutManager = layoutManager

        RecyclerManager.decorateRecyclerView(mylist,this)


        //  decoration = DividerItemDecoration(getContext(), VERTICAL)

        var offset = 10
        // val end =  StartOffsetItemDecoration(offset)
        //recycler.addItemDecoration()


        //setting animation on scrolling
        mylist.setHasFixedSize(true); //enhance recycler view scroll
        mylist.isNestedScrollingEnabled = false;// enable smooth scrooll


    }

    private fun updateContent(byLatestList: MutableList<Video>){

        searchAdapter.setHasStableIds(true)
        searchAdapter.notifyItemInserted(0)
        mylist.adapter  = searchAdapter

    }




    private fun loadHome(){
        val homeFragmentMgr: FragmentManager = supportFragmentManager
        homeFragmentMgr.beginTransaction()
            .replace(R.id.fragmentholder_botton, HomeFragment.newInstance())
            .commit()
    }


    private fun setUpBottomNavigation(){

        //BottomNavigationViewHelper.disableShiftMode(bottomNavigationView)

       bottom_navigation.setOnNavigationItemSelectedListener(this)




    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        selectBottomNavItem(item)
        return true
    }

    /*
    private class MyBottomItemsListner : BottomNavigationView.OnNavigationItemSelectedListener {
        override fun onNavigationItemSelected(@NonNull item: MenuItem): Boolean {
            item.isChecked = true
            selectBottomNavItem
            return true
        }
    }
     */


    fun selectBottomNavItem(item: MenuItem) {
        when (item.itemId) {
            R.id.menu_bottom_home -> {

              loadHome()

            }
            R.id.menu_bottom_discover -> {

             //   Toast.makeText(this, "Discover",Toast.LENGTH_LONG).show()

                val discoverFragmentMgr: FragmentManager = supportFragmentManager
                discoverFragmentMgr.beginTransaction()
                    .replace(R.id.fragmentholder_botton, DiscoverFragment.newInstance("",""))
                    .commit()



            }
            R.id.menu_bottom_account -> {
              //  Toast.makeText(this, "Account",Toast.LENGTH_LONG).show()

                val accountFragmentMgr = supportFragmentManager
                accountFragmentMgr.beginTransaction()
                    .replace(R.id.fragmentholder_botton, AccountFragment.newInstance())
                    .commit()

            }

        }
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            /*
            R.id.activity_menu_item ->  // Do Activity menu item stuff here
                return true
            R.id.fragment_menu_item ->  // Not implemented here
                return false
            else -> {
            }
             */
        }
        return false
    }


    override fun onVideoClick(view: View, pos: Int, video: Video) {
        
    }



}

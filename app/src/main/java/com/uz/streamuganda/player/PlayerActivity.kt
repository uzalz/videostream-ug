package com.uz.streamuganda.player

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.ui.PlayerView
import com.uz.streamuganda.R

class PlayerActivity : AppCompatActivity() {



    private  var exoPlayer : SimpleExoPlayer? = null
    private  var playerView : PlayerView? = null


    private var  playWhenReady = true
    private var currentWindow = 0
    private var playbackPosition : Long = 0



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_player)
    }
}

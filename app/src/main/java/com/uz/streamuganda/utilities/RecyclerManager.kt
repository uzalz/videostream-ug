package com.uz.streamuganda.utilities

import android.content.Context
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.BounceInterpolator
import android.view.animation.Interpolator
import android.view.animation.OvershootInterpolator
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.recyclerview.widget.RecyclerView
import com.uz.streamuganda.R

class RecyclerManager {




    companion object{

         fun decorateRecyclerView(recycler:RecyclerView, context: Context){

            //recyclerView.addItemDecoration(decoration);
            val overshoot: Interpolator = OvershootInterpolator(1f)
            val accelerateDecelerate: Interpolator = AccelerateDecelerateInterpolator()
            val bounce: Interpolator = BounceInterpolator()

            // val animator = SlideInUpAnimator(overshoot)
            // recyclerView.setItemAnimator(animator)
            // recyclerView.getItemAnimator().setAddDuration(resources.getInteger(R.integer.duration_recycler_view))
            //setting animation on scrolling

            //setting animation on scrolling
            //recycler.setHasFixedSize(true) //enhance recycler view scroll
            //recycler.setNestedScrollingEnabled(false) // enable smooth scrooll


            val itemDecoration = ItemOffsetDecoration(context, R.dimen.item_offset)
            recycler.addItemDecoration(itemDecoration)


        }

    }



}
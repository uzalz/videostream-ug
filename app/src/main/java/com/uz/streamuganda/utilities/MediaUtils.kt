package com.uz.streamuganda.utilities

import java.net.URLConnection


class MediaUtils {

    companion object {

        fun isVideoFile(path: String?): Boolean {
            val mimeType: String = URLConnection.guessContentTypeFromName(path)
            return mimeType != null && mimeType.startsWith("video")
        }

        fun isImageFile(path: String?): Boolean {
            val mimeType = URLConnection.guessContentTypeFromName(path)
            return mimeType != null && mimeType.startsWith("image")
        }
    }


}
package com.uz.streamuganda.utilities;

import android.content.Context;
import android.graphics.Typeface;

public class MyFonts {



    //private static final String workSansRegular = "fonts/workSans/WorkSans-Regular.otf";
    private static final String workSansMedium = "fonts/workSans/WorkSans-Medium.otf";
   // private static final String workSansSemiBold = "fonts/workSans/WorkSans-SemiBold.otf";
    private static final String workSansBold = "fonts/workSans/WorkSans-Bold.otf";
    //private static final String tradeGothic = "fonts/TradeGothic/TradeGothicLT.ttf";
    //private static final String thunder = "fonts/Thunder/Thunder.ttf";



    public static Typeface getTypeFace(Context context) {

        return Typeface.createFromAsset(context.getAssets(),  workSansMedium);
    }


    /*
    public static Typeface getBoldTypeFace(Context context) {

        return Typeface.createFromAsset(context.getAssets(), workSansBold);
    }

     */





}

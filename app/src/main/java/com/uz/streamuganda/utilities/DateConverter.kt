package com.uz.streamuganda.utilities

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class DateConverter {

    companion object {
        fun calculateDifferenceInDates(firstDate: Date, secondDate: Date): Long {
            val diffDays: Long
            //in milliseconds
            val diff = firstDate.time - secondDate.time
            val diffSeconds = diff / 1000 % 60
            val diffMinutes = diff / (60 * 1000) % 60
            val diffHours = diff / (60 * 60 * 1000) % 24
            diffDays = diff / (24 * 60 * 60 * 1000)
            return diffDays
        }

        fun stringToDateWithoutTime(date: String?): Date? {
            var date1: Date? = null
            val dateFormatter = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
            dateFormatter.timeZone = TimeZone.getTimeZone("UTC")
            //SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            try {
                date1 = dateFormatter.parse(date)
            } catch (e: ParseException) {
            }
            return date1
        }

        fun dateToStringWithoutTime(date : Date):String{

            var dateNoTime = ""
            val dateFormatter = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
            dateFormatter.timeZone = TimeZone.getTimeZone("UTC")

            //SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            dateNoTime = dateFormatter.format(date)
            return dateNoTime
        }



    }







}
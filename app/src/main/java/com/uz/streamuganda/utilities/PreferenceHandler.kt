package com.uz.streamuganda.utilities

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager

class PreferenceHandler(private val mContext: Context) {


        val sharedPreferencesInstance: SharedPreferences
            get() = PreferenceManager.getDefaultSharedPreferences(mContext)

        fun putPref(key: String?, value: String?) {
            val prefs = PreferenceManager.getDefaultSharedPreferences(mContext)
            val editor = prefs.edit()
            editor.putString(key, value)
            editor.apply()
        }

        fun getPref(key: String?): String? {
            val preferences =
                PreferenceManager.getDefaultSharedPreferences(mContext)
            return preferences.getString(key, null)
        }

        fun putLongPref(key: String?, value: Long) {
            val prefs = PreferenceManager.getDefaultSharedPreferences(mContext)
            val editor = prefs.edit()
            editor.putLong(key, value)
            editor.apply()
        }

        fun getLongPref(key: String?): Long {
            val preferences =
                PreferenceManager.getDefaultSharedPreferences(mContext)
            return preferences.getLong(key, 0)
        }

        fun putBoolPref(key: String?, value: Boolean) {
            val prefs = PreferenceManager.getDefaultSharedPreferences(mContext)
            val editor = prefs.edit()
            editor.putBoolean(key, value)
            editor.apply()
        }

        fun getBoolPref(key: String?): Boolean {
            val preferences =
                PreferenceManager.getDefaultSharedPreferences(mContext)
            return preferences.getBoolean(key, false)
        }



        fun setIntValue(key: String?, value: Int) {
            val prefs = PreferenceManager.getDefaultSharedPreferences(mContext)
            val editor = prefs.edit()
            editor.putInt(key, value)
            editor.apply()
        }

        fun getIntValue(key: String?): Int {
            val preferences =
                PreferenceManager.getDefaultSharedPreferences(mContext)
            return preferences.getInt(key, 0)
        }

        companion object {
            const val KEY_CHANNEL_ID = "key_channel_id"
            const val KEY_CHANNEL_NAME = "key_channel_name"
            const val KEY_CHANNEL_DESC = "key_channel_desc"

            const val KEY_PROFILE_ID = "key_profile_id"
            const val KEY_PROFILE_NAME = "key_profile_name"
            const val KEY_PROFILE_EMAIL = "key_profile_email"
            const val KEY_PROFILE_PHOTO_URL = "key_profile_photo_url"


            private var INSTANCE: PreferenceHandler? = null
            fun getInstance(context: Context): PreferenceHandler? {
                if (INSTANCE == null) {
                    INSTANCE = PreferenceHandler(context)
                }
                return INSTANCE
            }

    }


}
package com.uz.streamuganda.socialize

import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.StyleSpan
import com.uz.streamuganda.R
import kotlinx.android.synthetic.main.activity_create_review.*
import kotlinx.android.synthetic.main.activity_video_details.*
import kotlinx.android.synthetic.main.activity_video_details.collapsing_toolbar_video_details

class CreatReviewActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_review)

        setUpToolBar()
    }


    private fun setUpToolBar() {


        toolbar_create_review.setNavigationOnClickListener{

            finish()

        }

        //    toolbar_main.logo = resources.getDrawable(R.drawable.ic_logo)
        // toolbar_main.navigationIcon = resources.getDrawable(R.drawable.ic_arrow_back_black_24dp)
        setSupportActionBar(toolbar_create_review)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        //  supportActionBar!!.setIcon(R.drawable.ic_logo)
        supportActionBar!!.setDisplayShowTitleEnabled(true)

        val channelName = "Create Review"

        val channelNameSize = channelName.length

        val str = SpannableStringBuilder(channelName)
        str.setSpan(StyleSpan(Typeface.BOLD), 0, channelNameSize, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        toolbar_create_review.title = str

        supportActionBar!!.title = str


        ///collapsingToolbarLayout.setExpandedTitleGravity(Gravity.BOTTOM);
        ///collapsingToolbarLayout.setExpandedTitleGravity(Gravity.BOTTOM);

        // collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar)
        // collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar)
        //  collapsingToolbarLayout.setCollapsedTitleGravity(Gravity.START)
        collapsing_toolbar_video_details.isTitleEnabled = false
        //   collapsingToolbarLayout.setTitle("VimsApp")

    }

}

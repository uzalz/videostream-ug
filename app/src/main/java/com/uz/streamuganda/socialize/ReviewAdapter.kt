package com.uz.streamuganda.socialize

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.uz.streamuganda.R
import com.uzalz.service_facade.entities.Review
import com.uzalz.service_facade.entities.Video
import java.util.ArrayList


class ReviewAdapter(var context: Context, private var history: MutableList<Review>, private var contactslistner:ReviewVideolistner) : RecyclerView.Adapter<ReviewAdapter.ReviewHolder>() {




    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReviewHolder{

        val  view = LayoutInflater.from(parent.context).inflate(R.layout.item_review, parent, false)
        return ReviewHolder(view)
    }

    var isBind :Boolean = false

    //2. Our Adapter needs 2 ViewHolders
    class ReviewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var titleText: TextView = itemView.findViewById(R.id.tv_review_title)
        private var subtitleText: TextView = itemView.findViewById(R.id.tv_review_subtitle)
        private var reviewerText: TextView = itemView.findViewById(R.id.tv_review_reviewer)
        private var dateText: TextView = itemView.findViewById(R.id.tv_review_date)
        private var rateText: TextView = itemView.findViewById(R.id.tv_review_star_rating)



        fun bind(review: Review, reviewlistner: ReviewVideolistner) {



            titleText.text = review.title
            subtitleText.text =review.subtitle
            reviewerText.text = review.reviewer
            dateText.text = review.date
            rateText.text = review.rating

            val position = adapterPosition

            itemView.setOnClickListener {
             //   contactslistner.onVideoClick(itemView,position,video)

            }

        }

    }




    // // Passes the message object to a ViewHolder so that the contents can be bound to UI.
    override fun onBindViewHolder(holder: ReviewHolder, position: Int) {
        val review: Review = history[position]
        holder.bind(review,contactslistner)

    }

    override fun getItemCount(): Int {
        return history.size
    }



    fun getReview(pos: Int): Review {
        return history[pos]
    }





    //Clicklistner implementation
    interface ReviewVideolistner{

        fun onReviewClick(view: View, pos:Int, video: Video)

    }








}
package com.uz.streamuganda.discover

import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.StyleSpan
import android.util.Log
import androidx.appcompat.widget.Toolbar
import androidx.viewpager.widget.ViewPager
import com.uz.streamuganda.R
import com.uzalz.service_facade.APIClient
import com.uzalz.service_facade.discover.DiscoverApi
import com.uzalz.service_facade.discover.DiscoverRepository
import com.uzalz.service_facade.entities.Subscription
import com.uz.streamuganda.studio.ChannelAboutFragment
import com.uz.streamuganda.studio.ChannelHomeFragment
import com.uz.streamuganda.utilities.PreferenceHandler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_channel_details.*
import java.util.*

class ChannelDetailsActivity : AppCompatActivity() {

    var channelName = "Default"
    var channelDesc = ""
    var channelSub = ""
    var channelId = ""
    var isSub = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_channel_details)

        val bundle = intent.extras


        if(bundle!=null){
            channelId = bundle.getString("channelId").toString()
            channelName = bundle.getString("channelName").toString()
            channelDesc = bundle.getString("channelDesc").toString()
            channelSub = bundle.getInt("channelSubs").toString() +" subscriptions"
            isSub = bundle.getBoolean("isSub")

        }


        Log.d("ChannelDetailsNameNow","name: "+channelName)

        if(isSub){
            btn_channel_details_subscribe.text = "SUBSCRIBED"
            btn_channel_details_subscribe.icon = resources.getDrawable(R.drawable.ic_thumb_up_black_24dp)
            btn_channel_details_subscribe.isEnabled = false
        }

        setUpToolBar(channelName)
        setupViewPager()

        tv_channel_details_name.text = channelName
        tv_channel_details_subs.text = channelSub

    }




    private lateinit var  toolbar : Toolbar
    private fun setUpToolBar(channelName: String) {

        toolbar_channel_details.setNavigationOnClickListener{
            finish()
        }

        //    toolbar_main.logo = resources.getDrawable(R.drawable.ic_logo)
        // toolbar_main.navigationIcon = resources.getDrawable(R.drawable.ic_arrow_back_black_24dp)
        setSupportActionBar(toolbar_channel_details)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        //  supportActionBar!!.setIcon(R.drawable.ic_logo)
        supportActionBar!!.setDisplayShowTitleEnabled(true)

        val channelNameSize = channelName.length

        // val str = SpannableStringBuilder(channelName)
        // str.setSpan(StyleSpan(Typeface.BOLD), 0, channelNameSize, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        toolbar_channel_details.title = channelName

        supportActionBar!!.title = channelName




        //toolbar.setTitle("VimsApp");
        //toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimary));
        //((AppCompatActivity)getActivity()).getSupportActionBar().setLogo(R.drawable.notice_48);
        //  val collapsingToolbarLayout: CollapsingToolbarLayout = findViewById(R.id.collapsing_toolbar)

        ///collapsingToolbarLayout.setExpandedTitleGravity(Gravity.BOTTOM);
        ///collapsingToolbarLayout.setExpandedTitleGravity(Gravity.BOTTOM);

        // collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar)
        // collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar)
        //  collapsingToolbarLayout.setCollapsedTitleGravity(Gravity.START)
        //  collapsingToolbarLayout.setTitleEnabled(false)
        //   collapsingToolbarLayout.setTitle("VimsApp")

    }


    private fun setupViewPager() { //setup the viewpager--- it contains the fragments to be swiped
        val viewPager: ViewPager = viewpager_channel_details
        //assign viewpager to tablayout
        tabs_channel_details.setupWithViewPager(viewPager)

        val adapter = ViewPagerAdapter(supportFragmentManager) //child
        adapter.addFragment(ChannelDetailsHomeFragment.newInstance(channelId,channelName), "Home")
        adapter.addFragment(ChannelAboutFragment.newInstance(channelId,channelName,channelDesc), "About")
        viewPager.adapter = adapter



    }








}

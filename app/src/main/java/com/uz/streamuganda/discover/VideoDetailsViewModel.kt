package com.uz.streamuganda.discover

import android.os.Parcelable
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.android.parcel.Parcelize

@Parcelize
class VideoDetailsViewModel : ViewModel(), Parcelable {
    // TODO: Implement the ViewModel


    val mVideoTitle = MutableLiveData<String>()
    val videoTitleLiveData : LiveData<String> = mVideoTitle

    val mVideoDesc = MutableLiveData<String>()
    val videoDescLiveData : LiveData<String> = mVideoDesc

    val mVideoPrice = MutableLiveData<String>()
    val videoPriceLiveData : LiveData<String> = mVideoPrice






}

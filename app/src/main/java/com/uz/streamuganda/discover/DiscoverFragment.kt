package com.uz.streamuganda.discover

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mancj.materialsearchbar.MaterialSearchBar
import com.uz.streamuganda.R
import com.uz.streamuganda.utilities.AppExecutorsUI
import com.uz.streamuganda.utilities.MyFonts
import com.uz.streamuganda.utilities.RecyclerManager
import com.uz.streamuganda.utilities.RecyclerManager.Companion.decorateRecyclerView
import com.uzalz.service_facade.APIClient
import com.uzalz.service_facade.discover.DiscoverApi
import com.uzalz.service_facade.discover.DiscoverRepository
import com.uzalz.service_facade.entities.Channel
import com.uzalz.service_facade.entities.Video
import com.uzalz.service_facade.studio.StudioApi
import com.uzalz.service_facade.studio.StudioRepository


import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_discover.*
import kotlinx.android.synthetic.main.fragment_home.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [DiscoverFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class DiscoverFragment : Fragment(),VideoAdapter.Videolistner,ChannelAdapter.VideoChannellistner,CustomSuggestionsAdapter.SuggestionsVideolistner {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }


    private lateinit var tvRecommend :TextView


    private lateinit var recycler1 : RecyclerView
    private lateinit var recycler2 : RecyclerView
    private lateinit var recyclerLearn : RecyclerView
    private lateinit var recyclerEnt : RecyclerView
    private lateinit var recyclerFinance : RecyclerView


    private lateinit var searchBar: MaterialSearchBar

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {


        val v =  inflater.inflate(R.layout.fragment_discover, container, false)

        searchBar = v.findViewById<MaterialSearchBar>(R.id.searchBarDiscover)


        recycler1 = v.findViewById(R.id.video_list_1)
        recycler2 = v.findViewById(R.id.video_list_2)
        recyclerLearn = v.findViewById(R.id.video_list_learn)
        recyclerEnt = v.findViewById(R.id.video_list_ent)
       // recyclerFinance = v.findViewById(R.id.video_list_finance)

        tvRecommend = v.findViewById(R.id.tv_documentaries)
        addFonts()

        setUpRecyclerView1()
        setUpRecyclerView2()
        setUpRecyclerViewLearn()
        setUpRecyclerViewReligion()
        //setUpRecyclerViewFinance()

        getVideos()
        getChannels()


        setUpSearchBar()
        getSearchVideos()

        // Inflate the layout for this fragment
        return v
    }



    /////////////////////searchbar /////////////////////////////////////////////////
    private lateinit var suggestions : ArrayList<Video>
    private lateinit var customSuggestionsAdapter : CustomSuggestionsAdapter

    private fun setUpSearchBar(){
        // Search bar
      //  var searchBar = searchBarDiscover
        //enable searchbar callbacks
        //searchBar.setOnSearchActionListener(this);
        //restore last queries from disk
        //lastSearches = searchBar.getLastSuggestions();
        //searchBar.setLastSuggestions(lastSearches);
        //Inflate menu and setup OnMenuItemClickListener
        searchBar.inflateMenu(R.menu.main);
        //searchBar.setSearchIcon(R.drawable.ic_search);
        //searchBar.setRoundedSearchBarEnabled(true)
        //searchBar.toolbar_main.title = "TubeJet"
        //searchBar.setBackgroundColor(resources.getColor(R.color.windowBackground));
        searchBar.setCardViewElevation(20);
        //searchBar.setDividerColor(R.color.colorAccent);
        //searchBar.setTextColor(R.color.colorWhite)
        //searchBar.setPlaceHolderColor(getResources().getColor(R.color.light_blue));
        searchBar.setNavButtonEnabled(false);
        // SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        // var cn = ComponentName(this, SearchCaretakerActivity.class);
        // searchBar.enableSearch()
        searchBar.showSuggestionsList()

        searchBar.setOnSearchActionListener(object: MaterialSearchBar.OnSearchActionListener {
            override fun onButtonClicked(buttonCode: Int) {

                when (buttonCode) {
                    MaterialSearchBar.BUTTON_BACK -> searchCard.visibility = View.GONE
                    MaterialSearchBar.BUTTON_SPEECH -> searchCard.visibility = View.GONE
                }
            }

            override fun onSearchConfirmed(text: CharSequence?) {

            }

            override fun onSearchStateChanged(enabled: Boolean) {

            }


        })



        searchBar.addTextChangeListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                Log.d("LOG_MAIN_ACTIVITY",  " text changed " + searchBar.getText())
                // searchAdapter.getFilter().filter(searchBar.text);
                customSuggestionsAdapter.filter.filter(searchBar.text);
                // mylist.visibility=View.VISIBLE
                // searchCard.visibility = View.VISIBLE
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (TextUtils.isEmpty(s)){
                    //Text is cleared, do your thing
                    //  searchCard.visibility = View.GONE
                    Log.d("LOG_MAIN_ACTIVITY", " text changed empty now ")
                }

            }

        })

        setUpSearchBarAdapter()

    }

    private fun setUpSearchBarAdapter(){

        val inflater = requireActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        //searchAdapter = CustomSuggestionsAdapter(inflater)
        customSuggestionsAdapter = CustomSuggestionsAdapter(inflater,this)
        ///  suggestions = getSomeVideos()


    }
    fun getVideosFromChannels(vids: MutableList<Channel>):MutableList<Video>{
        var videos : MutableList<Video> = ArrayList()
        for(channel in vids){
            videos.addAll(channel.videos!!)
        }
        return videos
    }
    fun  getSearchVideos(){
        // 1. seperate videos by category named
        //   progressBar.visibility = View.VISIBLE

        var api = APIClient().getRetrofitInstance()!!.create(DiscoverApi::class.java)
        var discover =  DiscoverRepository(api)

        discover.getChannels()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object: DisposableObserver<MutableList<Channel>>() {

                override fun onNext(channels: MutableList<Channel>) {

                    var videos = getVideosFromChannels(channels)
                    customSuggestionsAdapter.suggestions = videos
                    searchBar.setCustomSuggestionAdapter(customSuggestionsAdapter)
                    // updateContent(latestVideos)
                }

                override fun onComplete() {

                }

                override fun onError(e: Throwable) {
                    // Toast.makeText(requireActivity(),e.message, Toast.LENGTH_LONG).show()

                }


            })

    }

    override fun onSuggestionsVideoClick(view: View, pos: Int, video: Video) {
     //   Toast.makeText(requireContext(),""+video.title,Toast.LENGTH_LONG).show()
        val intent  =  Intent(context, VideoDetailsActivity::class.java)

        intent.putExtra("videoTitle",video.title)
        intent.putExtra("videoDesc",video.description)
        intent.putExtra("videoPrice",video.price)
        var sub = ""+video.pgRating +" | "+video.category+" | "+video.year+" | "+video.runtime
        intent.putExtra("videoDetailsSubtitle", sub)
        intent.putExtra("videoContentId", video.contentId)
        intent.putExtra("videoUrl", video.videoUrl)
        intent.putExtra("videoPosterUrl", video.posterUrl)

        // intent.putExtras(bundle)
        startActivity(intent)


    }
   ////////////////////////////////////////////////////////////////////////////////



    private fun addFonts(){

        tvRecommend.typeface = MyFonts.getTypeFace(requireContext())


    }


    private fun setUpRecyclerView1(){

         val layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL,false)
        var gridColumns = 3
        //  val layoutManager =  GridLayoutManager(activity, gridColumns, GridLayoutManager.VERTICAL, false)



        recycler1.layoutManager = layoutManager
        decorateRecyclerView(recycler1,requireContext())

        //  decoration = DividerItemDecoration(getContext(), VERTICAL)

        var offset = 10
        // val end =  StartOffsetItemDecoration(offset)
        //recycler.addItemDecoration()


        //setting animation on scrolling
       // recycler1.setHasFixedSize(true); //enhance recycler view scroll
        recycler1.isNestedScrollingEnabled = false;// enable smooth scrooll
    }

    private fun setUpRecyclerView2(){

         val layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL,false)
         var gridColumns = 3
       // val layoutManager =  GridLayoutManager(activity, gridColumns, GridLayoutManager.VERTICAL, false)


        recycler2.layoutManager = layoutManager
        decorateRecyclerView(recycler2,requireContext())

        //decoration = DividerItemDecoration(getContext(), VERTICAL)

        var offset = 10
        // val end =  StartOffsetItemDecoration(offset)
        //recycler.addItemDecoration()

        //setting animation on scrolling
        //recycler2.setHasFixedSize(true); //enhance recycler view scroll
        recycler2.isNestedScrollingEnabled = false;// enable smooth scrooll

    }

    private fun setUpRecyclerViewLearn(){

        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL,false)
        var gridColumns = 3
        // val layoutManager =  GridLayoutManager(activity, gridColumns, GridLayoutManager.VERTICAL, false)

        recyclerLearn.layoutManager = layoutManager
        decorateRecyclerView(recyclerLearn,requireContext())

        //decoration = DividerItemDecoration(getContext(), VERTICAL)

        var offset = 10
        // val end =  StartOffsetItemDecoration(offset)
        //recycler.addItemDecoration()

        //setting animation on scrolling
        //recyclerLearn.setHasFixedSize(true); //enhance recycler view scroll
        recyclerLearn.isNestedScrollingEnabled = false;// enable smooth scrooll

    }

    private fun setUpRecyclerViewReligion(){

        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL,false)
        var gridColumns = 3
        // val layoutManager =  GridLayoutManager(activity, gridColumns, GridLayoutManager.VERTICAL, false)

        recyclerEnt.layoutManager = layoutManager
        decorateRecyclerView(recyclerEnt,requireContext())


        // decoration = DividerItemDecoration(getContext(), VERTICAL)

        var offset = 10
        // val end =  StartOffsetItemDecoration(offset)
        // recycler.addItemDecoration()

        //setting animation on scrolling
        //recyclerReligion.setHasFixedSize(true); //enhance recycler view scroll
        recyclerEnt.isNestedScrollingEnabled = false;// enable smooth scroll

    }

    private fun setUpRecyclerViewFinance(){

        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL,false)
        var gridColumns = 3
        // val layoutManager =  GridLayoutManager(activity, gridColumns, GridLayoutManager.VERTICAL, false)

        recyclerFinance.layoutManager = layoutManager

        //decoration = DividerItemDecoration(getContext(), VERTICAL)

        var offset = 10
        // val end =  StartOffsetItemDecoration(offset)
        //recycler.addItemDecoration()

        //setting animation on scrolling
        //recyclerFinance.setHasFixedSize(true); //enhance recycler view scroll
        recyclerFinance.isNestedScrollingEnabled = false;// enable smooth scrooll

    }


    var appExecutors = AppExecutorsUI()
    private fun updateChannels(t: MutableList<Channel>){
       var run = Runnable {

           updateChannelContent(t)
       }

        appExecutors.mainThread().execute(run)



    }



    fun  getChannels(){

        var api = APIClient().getRetrofitInstance()!!.create(StudioApi::class.java)
        var studio =  StudioRepository(api)

        studio.getChannels()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object: DisposableObserver<MutableList<Channel>>() {

                override fun onNext(t: MutableList<Channel>) {

                  updateChannels(t)
                }

                override fun onComplete() {

                }

                override fun onError(e: Throwable) {
                  //  Toast.makeText(requireContext(),e.message, Toast.LENGTH_LONG).show()
                }


            })

    }


    fun byDocumentary(category:String,channels: MutableList<Channel>):MutableList<Video>{
        var videos : MutableList<Video> = ArrayList()

        for(c in  channels) {
            for (video in c.videos!!) {
                if (video.category == category) {
                    videos.add(video)
                }
            }
        }
        return videos
    }


    fun  getVideos(){
        //1. seperate videos by category named

        var api = APIClient().getRetrofitInstance()!!.create(DiscoverApi::class.java)
        var studio =  DiscoverRepository(api)

        studio.getChannels()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object: DisposableObserver<MutableList<Channel>>() {

                override fun onNext(t: MutableList<Channel>) {

                    var ent = byDocumentary("Entertainment",t)
                    var educ = byDocumentary("Education",t)

                        updateVideoContent(ent,educ)
                }

                override fun onComplete() {

                }

                override fun onError(e: Throwable) {
                 //   Toast.makeText(requireContext(),e.message, Toast.LENGTH_LONG).show()
                }


            })

    }


    override fun onResume() {
        super.onResume()

            getVideos()
            getChannels()

    }


    private fun updateVideoContent(byEnt: MutableList<Video>, byEduc: MutableList<Video>){
        val context = context ?: return // early return using Elvis operator

        var run = Runnable {
            val adapter = VideoAdapter(context,byEduc, this)
            adapter.setHasStableIds(true)
            adapter.notifyItemInserted(0)
            recycler1.adapter  = adapter


            val adapter2 = VideoAdapter(context,byEduc, this)
            adapter2.setHasStableIds(true)
            adapter2.notifyItemInserted(0)
            recyclerLearn.adapter  = adapter2

            val adapter3 = VideoAdapter(context,byEnt, this)
            adapter3.setHasStableIds(true)
            adapter3.notifyItemInserted(0)
            recyclerEnt.adapter  = adapter3
        }
        AppExecutorsUI().mainThread().execute(run)


           /*
            val adapter4 = VideoAdapter(context!!,byEduc, this)
            adapter4.setHasStableIds(true)
            adapter4.notifyItemInserted(0)
            recyclerFinance.adapter  = adapter4
           */

    }


    private fun updateChannelContent(videoList: MutableList<Channel>){
        val context = context ?: return // early return using Elvis operator

        val adapter = ChannelAdapter(requireContext(),videoList, this)
        adapter.setHasStableIds(true)
        adapter.notifyItemInserted(0)
        recycler2.adapter  = adapter

     }



    override fun onVideoClick(view: View, pos: Int, video: Video) {

      //  Toast.makeText(context,video.title,Toast.LENGTH_LONG).show()

        val intent  =  Intent(context, VideoDetailsActivity::class.java)

       // Log.d("DiscoverFragment","vid: "+video.description)

        intent.putExtra("videoTitle",video.title)
        intent.putExtra("videoDesc",video.description)
        intent.putExtra("videoPrice",video.price)
        var sub = ""+video.pgRating +" | "+video.category+" | "+video.year+" | "+video.runtime
        intent.putExtra("videoDetailsSubtitle", sub)
        intent.putExtra("videoContentId", video.contentId)
        intent.putExtra("videoUrl", video.videoUrl)
        intent.putExtra("videoPosterUrl", video.posterUrl)

        // intent.putExtras(bundle)
        startActivity(intent)


    }

    override fun onChannelClick(view: View, pos: Int, channel: Channel,isSubscribed:Boolean) {


            //   bundle.putString("channesl",gson.toString())
            val intent = Intent(context, ChannelDetailsActivity::class.java)
            intent.putExtra("channelId",channel.channelId)
            intent.putExtra("channelName",channel.channelName)
            intent.putExtra("channelDesc",channel.channelDescription)
            intent.putExtra("channelSubs", channel.getNoSubscriptions())
            intent.putExtra("isSub", isSubscribed)
          //  Log.d("ChannelAdapter","Adapter2: "+isSub)


        startActivity(intent)

    }



    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment DiscoverFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            DiscoverFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}

package com.uz.streamuganda.discover

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.net.Uri
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.StyleSpan
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.makeramen.roundedimageview.RoundedTransformationBuilder
import com.squareup.picasso.Picasso
import com.squareup.picasso.Transformation
import com.theoplayer.android.api.THEOplayerView
import com.uz.streamuganda.player.DacastPlayer
import com.uz.streamuganda.R
import com.uz.streamuganda.socialize.CreatReviewActivity
import com.uz.streamuganda.socialize.ReviewAdapter
import com.uz.streamuganda.utilities.RecyclerManager
import com.uzalz.service_facade.APIClient
import com.uzalz.service_facade.entities.Channel
import com.uzalz.service_facade.entities.Review
import com.uzalz.service_facade.entities.Video
import com.uzalz.service_facade.studio.StudioApi
import com.uzalz.service_facade.studio.StudioRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_video_details.*
import java.util.*
import kotlin.collections.ArrayList


class VideoDetailsActivity : AppCompatActivity(),ReviewAdapter.ReviewVideolistner {



    lateinit var dacastPlayer : DacastPlayer
    lateinit var theoPlayerView: THEOplayerView


    /////////////////  EXO PLAYER /////////////////////////

    private  var exoPlayer : SimpleExoPlayer? = null
    private  var playerView : PlayerView? = null


    private var  playWhenReady = true
    private var currentWindow = 0
    private var playbackPosition : Long = 0

    ///////////////////////////////////////////////////////


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video_details)


        val bundle = intent.extras
        var title = bundle?.get("videoTitle").toString()
        var desc = bundle?.get("videoDesc").toString()
        var price = bundle?.get("videoPrice").toString()
        var details = bundle?.get("videoDetailsSubtitle").toString()

        var posterUrl = bundle?.get("videoPosterUrl").toString()
        var contentId = bundle?.get("videoContentId").toString()

        var videoUrl = bundle?.get("videoUrl").toString()
        var priceLabel = resources.getString(R.string.label_video_details_button_buy)+" "+price

        setUpToolBar(title)

        var newDesc = "$desc $videoUrl"

        updateUI(title,desc,priceLabel, details, posterUrl)

        //  theoPlayerView = theoplayer
        //  initialiseExoPlayer()
        setUpDacast()

        setUpRecyclerView()
        updateContent(getMyReviews())
        review()
        openCloseReview()

    }



    var shareCode ="https://iframe.dacast.com/vod/7892a954-5e9f-42cf-caed-41857a6164d8/a0e8fba2-a852-4c23-76dc-c08eb7918bc1"
    var videoContentId = "7892a954-5e9f-42cf-caed-41857a6164d8-vod-a0e8fba2-a852-4c23-76dc-c08eb7918bc1"
    var vId = "104301_f_506288"

    var contentId = "cb8ba05e-150f-3dd2-11e8-6fcb6ccd8f6e-vod-de5cbcb0-f36c-afc2-f6df-a1d50ae1c97d"

    fun setUpDacast(){
        //Initialise player
        dacastPlayer = DacastPlayer(
            this, contentId /*, "https://cdn.theoplayer.com/demos/preroll.xml" */
        )

        dacastPlayer.view.layoutParams = ConstraintLayout.LayoutParams(
            ConstraintLayout.LayoutParams.MATCH_PARENT,
            ConstraintLayout.LayoutParams.MATCH_PARENT
        )

        //playerView?.addView(dacastPlayer.view)
        layout_dacast.addView(dacastPlayer.view)

        //Get instance of the O player
        var thePlayer =  dacastPlayer.theOplayer
       // thePlayer.display

        video_details_buy.setOnClickListener{

            thePlayer.player.play()

        }


    }


    private fun setUpToolBar(channelName: String) {


        toolbar_video_details.setNavigationOnClickListener{

            finish()

        }

        //    toolbar_main.logo = resources.getDrawable(R.drawable.ic_logo)
        // toolbar_main.navigationIcon = resources.getDrawable(R.drawable.ic_arrow_back_black_24dp)
        setSupportActionBar(toolbar_video_details)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        //  supportActionBar!!.setIcon(R.drawable.ic_logo)
        supportActionBar!!.setDisplayShowTitleEnabled(true)

        val channelNameSize = channelName.length

        val str = SpannableStringBuilder(channelName)
        str.setSpan(StyleSpan(Typeface.BOLD), 0, channelNameSize, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        toolbar_video_details.title = str

        supportActionBar!!.title = str


        ///collapsingToolbarLayout.setExpandedTitleGravity(Gravity.BOTTOM);
        ///collapsingToolbarLayout.setExpandedTitleGravity(Gravity.BOTTOM);

        // collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar)
        // collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar)
        //  collapsingToolbarLayout.setCollapsedTitleGravity(Gravity.START)
        collapsing_toolbar_video_details.isTitleEnabled = false
        //   collapsingToolbarLayout.setTitle("VimsApp")

    }


    fun updateUI(title:String,desc:String,price:String,details:String, posterUrl:String){

        video_details_title.text = title
        video_details_desc.text = desc
        video_details_subtitle.text = details
        video_details_buy.text = price



        val transformation: Transformation = RoundedTransformationBuilder()
            .borderColor(Color.BLACK)
            .borderWidthDp(0.5f)
            .cornerRadiusDp(5f)
            .oval(false)
            .build()

        Picasso.with(this).load(posterUrl)
            .fit().centerCrop()
            .transform(transformation)
            .placeholder(R.drawable.ic_movie)
            //.error(R.drawable.user_placeholder_error)
            .into(video_details_profile_image)

    }


    private fun openCloseReview(){

        icon_review_drop_down.setOnClickListener {

            recycler_reviews.visibility = View.VISIBLE
            icon_review_drop_up.visibility = View.VISIBLE
            icon_review_drop_down.visibility = View.GONE
        }

        icon_review_drop_up.setOnClickListener {

            recycler_reviews.visibility = View.GONE
            icon_review_drop_down.visibility = View.VISIBLE
            icon_review_drop_up.visibility = View.GONE
        }


    }


    fun  getChannel(){

        var api = APIClient().getRetrofitInstance()!!.create(StudioApi::class.java)
        var studio =  StudioRepository(api)


        studio.getChannel("")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object: DisposableObserver<Channel>() {

                override fun onNext(t: Channel) {

                    // Log.d("ChannelHome"," name: "+t.channelName+" sizevideos: "+t.videos!!.size)
                    // progressBar.visibility = View.INVISIBLE


                }

                override fun onComplete() {

                }

                override fun onError(e: Throwable) {
                   // progressBar.visibility = View.INVISIBLE
                   // emptyLayout.visibility = View.VISIBLE
                   // recyclerView.visibility = View.INVISIBLE
                }


            })
    }



    /////////////////////// EXO PLAYER SETUP ///////////////////////////////////////


    private fun initialiseExoPlayer(){


        /*3.Our player can hog a lot of resources including memory, CPU, network connections and hardware codecs.
         Many of these resources are in short supply, particularly for hardware codecs where there may only be one.
          It's important that we release those resources for other apps to use when we're not using them, such as
           when our app is put into the background. Put another way, our player's lifecycle should be tied to the
            lifecycle of our app. To implement this, you'll need to override the four methods onStart, onResume,
            onPause and onStop of PlayerActivity.
         */
        /*
    Starting with API level 24 Android supports multiple windows. As our app can be visible but not active in split window mode,
    we need to initialize the player in onStart. Before API level 24 we wait as long as possible until we grab resources,
     so we wait until onResume before initializing the player.
     */

        playerView = video_view

        if(exoPlayer == null){
            exoPlayer = SimpleExoPlayer.Builder(this).build()
        }


        //set player on player view
        playerView?.player = exoPlayer

        //2. Create a media source - your player now needs some content to play create a media item
        // A progressive media source is one which is obtained through progressive download.

        // val mp3file = " https://storage.googleapis.com/exoplayer-test-media-0/play.mp3"

        var media = getString(R.string.media_url_mp4)
        var media2 = "https://storage.googleapis.com/muxdemofiles/mux-video-intro.mp4"

        val uri = Uri.parse(media2)
        val mediaSource = buildMediaSource(uri)


        //Start playing as soon all resources for playback have been acquired, since playwhenready is nitially true
        // playback will start automatically the first time the app is run
        exoPlayer?.playWhenReady = playWhenReady

        //tells the player to seek to a certain position within a specific window. Both currentWindow and playbackPosition
        //  were initialized to zero so that playback starts from the very start the first time the app is run.
        exoPlayer?.seekTo(currentWindow, playbackPosition)

        // tells the player to acquire all the resources for the given mediaSource, and additionally tells it not to
        // reset the position or state, since we already set these in the two previous lines.
        exoPlayer?.prepare(mediaSource!!, false, false)



    }

    private fun buildMediaSource(uri: Uri): MediaSource? {
        // a factory of creating progressive mdeia data sources
        val dataSourceFactory: DataSource.Factory = DefaultDataSourceFactory(this, "exoplayer-codelab")
        return ProgressiveMediaSource.Factory(dataSourceFactory)
            .createMediaSource(uri)


        /*
        Multimedia data is usually stored using a container format, such as MP4 or Ogg. Before the video and/or audio
         data can be played it must be extracted from the container. ExoPlayer is capable of
        extracting data from many different container formats using a variety of Extractor classes.
         */
    }


    // END OF EXO PLAYER SETUP /////////////////////////////////////////////////////






    override fun onStart() {
        super.onStart()

        if(Util.SDK_INT > 23){

       //     initialisePlayer()
        }


    }


    /*
    Before API Level 24 there is no guarantee of onStop being called. So we have to release the player as early
    as possible in onPause. Starting with API Level 24 (which brought multi and split window mode) onStop is
    guaranteed to be called. In the paused state our activity is still visible so we wait to release the player until onStop.

     We now need to create a releasePlayer method which frees up the player's resources and destroys it.

     */
    override fun onStop() {
        super.onStop()
        if (Util.SDK_INT > 23) {
         //   releaseExoPlayer()
        }
    }

    override fun onPause() {
        super.onPause()
         dacastPlayer.onPause()

        if (Util.SDK_INT <= 23) {
           // releaseExoPlayer()
        }
    }

    override fun onResume() {
        super.onResume()
        dacastPlayer.onResume()

        hideSystemUi()

        if(Util.SDK_INT <= 23 || exoPlayer == null){

           // initialiseeExoPlayer()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        dacastPlayer.onDestroy()
    }


    @SuppressLint("InlinedApi")
    private fun hideSystemUi() {
        playerView?.systemUiVisibility = (View.SYSTEM_UI_FLAG_LOW_PROFILE
                or View.SYSTEM_UI_FLAG_FULLSCREEN
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION)
    }






    /*
    Before we release and destroy the player we store the following information:

    play/pause state using getPlayWhenReady
    current playback position using getCurrentPosition
    current window index using getCurrentWindowIndex. Windows are explained in more detail here.

This will allow us to resume playback from where the user left off.
All we need to do is supply this state information when we initialize our player.

     */




    private fun releaseExoPlayer() {
        if (exoPlayer != null) {
            playWhenReady = exoPlayer?.playWhenReady!!
            playbackPosition = exoPlayer?.currentPosition!!
            currentWindow = exoPlayer?.currentWindowIndex!!
            exoPlayer?.release()
            exoPlayer = null;
        }
    }




    //RATING
    private fun setUpRecyclerView(){

        val staggeredGridLayoutManager =
            StaggeredGridLayoutManager(3, LinearLayoutManager.VERTICAL)

         val layoutManager = LinearLayoutManager(this)
        var gridColumns = 2
       // val layoutManager =  GridLayoutManager(this, gridColumns, GridLayoutManager.VERTICAL, false)

        recycler_reviews.layoutManager = layoutManager

        RecyclerManager.decorateRecyclerView(recycler_reviews,this)

        //  decoration = DividerItemDecoration(getContext(), VERTICAL)

        var offset = 10
        // val end =  StartOffsetItemDecoration(offset)
        //recycler.addItemDecoration()

        //setting animation on scrolling
        recycler_reviews.setHasFixedSize(true); //enhance recycler view scroll
        recycler_reviews.isNestedScrollingEnabled = false;// enable smooth scrooll

    }

    private fun updateContent(byLatestList: MutableList<Review>){

        val adapter = ReviewAdapter(this,byLatestList, this)
        adapter.setHasStableIds(true)
        adapter.notifyItemInserted(0)
        recycler_reviews.adapter  = adapter
    }


    override fun onReviewClick(view: View, pos: Int, video: Video) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }



    private fun getMyReviews():ArrayList<Review>{
      var list = ArrayList<Review>()
        var review = Review()
        review.date = Date().toString()
        review.title = "Calamari by your name"
        review.rating = "9/10"
        review.reviewer = "thomasbgleeson"
        review.subtitle = "Beautiful animation and a story of live and friendship. This is a wonderful film with a somewhat " +
                "predictable plot but a short runtime. its a pleasure to spend time in this universe"


        var review1 = Review()
        review1.date = Date().toString()
        review1.title = "called by your name"
        review1.rating = "8/10"
        review1.reviewer = "lasson"
        review1.subtitle = "Beautiful animation and a story of live and friendship. This is a wonderful film with a somewhat " +
                "predictable plot but a short runtime. its a pleasure to spend time in this universe"


        list.add(review)
        list.add(review1)
        return  list

    }


    private fun review(){
       reviewTitle.setOnClickListener{

           val intent = Intent(this,CreatReviewActivity::class.java)
           startActivity(intent)

       }

    }

}

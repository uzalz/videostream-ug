package com.uz.streamuganda.discover


/**
 * A simple [Fragment] subclass.
 */

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.BounceInterpolator
import android.view.animation.Interpolator
import android.view.animation.OvershootInterpolator
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.uz.streamuganda.R
import com.uz.streamuganda.utilities.AppExecutorsUI
import com.uz.streamuganda.utilities.DateConverter.Companion.calculateDifferenceInDates
import com.uz.streamuganda.utilities.ItemOffsetDecoration
import com.uz.streamuganda.utilities.RecyclerManager
import com.uzalz.service_facade.APIClient
import com.uzalz.service_facade.discover.DiscoverApi
import com.uzalz.service_facade.discover.DiscoverRepository
import com.uzalz.service_facade.entities.Channel
import com.uzalz.service_facade.entities.Video
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_home_new.view.*
import java.util.*
import kotlin.collections.ArrayList


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [MovieFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MovieFragment : Fragment(),VideoAdapter.Videolistner {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    lateinit var recycler : RecyclerView
    lateinit var progressBar : ProgressBar
    lateinit var layout_empty : LinearLayout

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_home_new, container, false)

        recycler = v.findViewById(R.id.video_list)
        progressBar = v.progressBarHomeNewVideos
        layout_empty = v.layout_home_videos_title

        setUpRecyclerView()
        getVideos()

        return v

    }

    override fun onResume() {
        super.onResume()
       // getVideos()
    }



    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment MovieFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            MovieFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    /*
     0772427452
    0701119896
     */


    /////////////////////// SEARCH BAR ////////////////////////////////////////////////////////





    fun byLatest(vids: MutableList<Video>):MutableList<Video>{
        var videos : MutableList<Video> = ArrayList()
        for(video in vids){

            val dateNow = Date()
            val dateLater = video.releaseDate
            var days = calculateDifferenceInDates(dateNow,dateLater!!)
            if(days < 14 && days >= 0){
                videos.add(video)
                Log.d("NewMoviesFragment","video name: "+video.title+" video price"+video.price)

            }

            Log.d("NewMoviesFragment","date diff: "+days+" later:"+dateLater)

        }
        return videos
    }

    fun getVideosFromChannels(vids: MutableList<Channel>):MutableList<Video>{
        var videos : MutableList<Video> = ArrayList()
        for(channel in vids){
            videos.addAll(channel.videos!!)
        }
        return videos
    }

    fun  getVideos(){
        // 1. seperate videos by category named
        progressBar.visibility = View.VISIBLE

        var api = APIClient().getRetrofitInstance()!!.create(DiscoverApi::class.java)
        var discover =  DiscoverRepository(api)

        discover.getChannels()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object: DisposableObserver< MutableList<Channel>>() {

                override fun onNext(channels: MutableList<Channel>) {

                         var videos = getVideosFromChannels(channels)
                         var latestVideos = byLatest(videos)


                          var run = Runnable {

                              if(latestVideos.isNotEmpty()){

                                  progressBar.visibility = View.INVISIBLE
                                  recycler.visibility = View.VISIBLE
                                  layout_empty.visibility = View.INVISIBLE

                                  updateContent(latestVideos)

                              }else{

                                  progressBar.visibility = View.INVISIBLE
                                  recycler.visibility = View.INVISIBLE
                                  layout_empty.visibility = View.VISIBLE

                              }
                          }
                          AppExecutorsUI().mainThread().execute(run)


                }

                override fun onComplete() {

                }

                override fun onError(e: Throwable) {
                   // Toast.makeText(requireActivity(),e.message, Toast.LENGTH_LONG).show()

                }


            })

    }

    private fun updateContent(byLatestList: MutableList<Video>){

        val adapter = VideoAdapter(context!!,byLatestList, this)
        adapter.setHasStableIds(true)
        adapter.notifyItemInserted(0)
        recycler.adapter  = adapter
    }

    private fun setUpRecyclerView(){

        val staggeredGridLayoutManager =
            StaggeredGridLayoutManager(3, LinearLayoutManager.VERTICAL)

        // val layoutManager = LinearLayoutManager(context)
        var gridColumns = 2
        val layoutManager =  GridLayoutManager(activity, gridColumns, GridLayoutManager.VERTICAL, false)

        recycler.layoutManager = staggeredGridLayoutManager

        RecyclerManager.decorateRecyclerView(recycler,requireContext())

        //  decoration = DividerItemDecoration(getContext(), VERTICAL)

        var offset = 10
       // val end =  StartOffsetItemDecoration(offset)
        //recycler.addItemDecoration()


        //setting animation on scrolling
        recycler.setHasFixedSize(true); //enhance recycler view scroll
        recycler.isNestedScrollingEnabled = false;// enable smooth scrooll


    }



    override fun onVideoClick(view: View, pos: Int, video: Video) {

        // Toast.makeText(context,video.title,Toast.LENGTH_LONG).show()

         val intent  =  Intent(context, VideoDetailsActivity::class.java)

        intent.putExtra("videoTitle",video.title)
        intent.putExtra("videoDesc",video.description)
        intent.putExtra("videoPrice",video.price)
        var sub = ""+video.pgRating +" | "+video.category+" | "+video.year+" | "+video.runtime
        intent.putExtra("videoDetailsSubtitle", sub)
        intent.putExtra("videoContentId", video.contentId)
        intent.putExtra("videoUrl", video.videoUrl)
        intent.putExtra("videoPosterUrl", video.posterUrl)


        // intent.putExtras(bundle)
         startActivity(intent)

    }




}


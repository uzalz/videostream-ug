package com.uz.streamuganda.discover

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.uz.streamuganda.R
import com.uzalz.service_facade.entities.Channel
import com.uz.streamuganda.utilities.PreferenceHandler

class ChannelAdapter(private var context: Context, private var history: List<Channel>, private var contactslistner: VideoChannellistner) : RecyclerView.Adapter<ChannelAdapter.VideoHolder>() {




    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoHolder {

        val  view = LayoutInflater.from(parent.context).inflate(R.layout.item_channel, parent, false)
        return VideoHolder(view)
    }



    //2. Our Adapter needs 2 ViewHolders
    class VideoHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var titleText: TextView = itemView.findViewById(R.id.item_channel_name)
        private var subtitleText: TextView = itemView.findViewById(R.id.item_channel_desc)
        private var videoPoster: ImageView = itemView.findViewById(R.id.video_poster)


        fun bind(isSubscribed:Boolean,channel: Channel, contactslistner: VideoChannellistner) {

             var channelName = channel.channelName!!

            if(channelName.length > 10){
              var newTitle =  channelName.substring(0,9)
                titleText.text = newTitle
            }else{
                titleText.text = channelName
            }

            val sub = channel.subscriptions?.size.toString()+" subs"
            subtitleText.text = sub
            // for the moment placeholder image
            // videoPoster.setImageResource(video.hardcover!!)

            val position = adapterPosition

            itemView.setOnClickListener {
                contactslistner.onChannelClick(itemView,position,channel,isSubscribed)

            }

        }

    }


    // Passes the message object to a ViewHolder so that the contents can be bound to UI.
    override fun onBindViewHolder(holder: VideoHolder, position: Int) {
        val video: Channel = history[position]
        val isSub = isSubscribed(video)
        Log.d("ChannelAdapter","Adapter: "+isSub)

        holder.bind(isSub,video,contactslistner)
    }

    override fun getItemCount(): Int {
        return history.size
    }

    fun isSubscribed(channel: Channel):Boolean{
        var id = PreferenceHandler(context).getPref(PreferenceHandler.KEY_PROFILE_ID)

        var isSub = false
        for(sub in channel.subscriptions!!){
            if(id == sub.userId){
                isSub = true
            }
        }
        return isSub
    }



    fun getVideo(pos: Int): Channel {
        return history[pos]
    }


    // Clicklistner implementation
    interface VideoChannellistner{

        fun onChannelClick(view: View, pos:Int, channel: Channel,isSubscribed:Boolean)

    }



}
package com.uz.streamuganda.discover

import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat.getSystemService
import androidx.viewpager.widget.ViewPager
import com.mancj.materialsearchbar.MaterialSearchBar
import com.uz.streamuganda.R
import com.uzalz.service_facade.APIClient
import com.uzalz.service_facade.discover.DiscoverApi
import com.uzalz.service_facade.discover.DiscoverRepository
import com.uzalz.service_facade.entities.Channel
import com.uzalz.service_facade.entities.Video
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_home.*


import kotlinx.android.synthetic.main.home_fragment.*
import kotlinx.android.synthetic.main.home_fragment.tabs_movie
import kotlinx.android.synthetic.main.home_fragment.viewpager_home

class HomeFragment : Fragment(),CustomSuggestionsAdapter.SuggestionsVideolistner {

    companion object {
        fun newInstance() = HomeFragment()
    }

    private lateinit var viewModel: HomeViewModel
    private lateinit var searchBar: MaterialSearchBar

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        val view =  inflater.inflate(R.layout.fragment_home, container, false)

        searchBar = view.findViewById<MaterialSearchBar>(R.id.searchBarHome)

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        // TODO: Use the ViewModel

        setupViewPager()
        setUpSearchBar()
        setUpSearchBarAdapter()
        getVideos()
    }



    private fun setupViewPager() { //setup the viewpager--- it contains the fragments to be swiped
        val viewPager: ViewPager = viewpager_home
        //assign viewpager to tablayout
        tabs_movie.setupWithViewPager(viewPager)

           val adapter = ViewPagerAdapter(childFragmentManager) //child
            adapter.addFragment( MovieFragment.newInstance("",""), "Latest")
            adapter.addFragment(TrendingMoviesFragment.newInstance("",""), "Trending")
            viewPager.setAdapter(adapter)
    }


    private lateinit var suggestions : ArrayList<Video>
    private lateinit var customSuggestionsAdapter : CustomSuggestionsAdapter


    private fun setUpSearchBar(){
        // Search bar
        var searchBar = searchBarHome
        //enable searchbar callbacks
        //searchBar.setOnSearchActionListener(this);
        //restore last queries from disk
        //lastSearches = searchBar.getLastSuggestions();
        //searchBar.setLastSuggestions(lastSearches);
        //Inflate menu and setup OnMenuItemClickListener
        searchBar.inflateMenu(R.menu.main);
        //searchBar.setSearchIcon(R.drawable.ic_search);
        //searchBar.setRoundedSearchBarEnabled(true)
        //searchBar.toolbar_main.title = "TubeJet"
        //searchBar.setBackgroundColor(resources.getColor(R.color.windowBackground));
        searchBar.setCardViewElevation(20);
        //searchBar.setDividerColor(R.color.colorAccent);
        //searchBar.setTextColor(R.color.colorWhite)
        //searchBar.setPlaceHolderColor(getResources().getColor(R.color.light_blue));
        searchBar.setNavButtonEnabled(false);
        // SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        // var cn = ComponentName(this, SearchCaretakerActivity.class);
        // searchBar.enableSearch()
        searchBar.showSuggestionsList()

        searchBar.setOnSearchActionListener(object: MaterialSearchBar.OnSearchActionListener {
            override fun onButtonClicked(buttonCode: Int) {

                when (buttonCode) {
                    MaterialSearchBar.BUTTON_BACK -> searchCard.visibility = View.GONE
                    MaterialSearchBar.BUTTON_SPEECH -> searchCard.visibility = View.GONE
                }
            }

            override fun onSearchConfirmed(text: CharSequence?) {

            }

            override fun onSearchStateChanged(enabled: Boolean) {

            }


        })



        searchBar.addTextChangeListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                Log.d("LOG_MAIN_ACTIVITY",  " text changed " + searchBar.getText())
                // searchAdapter.getFilter().filter(searchBar.text);
                customSuggestionsAdapter.filter.filter(searchBar.text);
                // mylist.visibility=View.VISIBLE
                // searchCard.visibility = View.VISIBLE
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (TextUtils.isEmpty(s)){
                    //Text is cleared, do your thing
                  //  searchCard.visibility = View.GONE
                    Log.d("LOG_MAIN_ACTIVITY", " text changed empty now ")
                }

            }

        })

    }



    private fun setUpSearchBarAdapter(){

        val inflater = requireActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        //searchAdapter = CustomSuggestionsAdapter(inflater)
        customSuggestionsAdapter = CustomSuggestionsAdapter(inflater,this)
      ///  suggestions = getSomeVideos()


    }




    fun getVideosFromChannels(vids: MutableList<Channel>):MutableList<Video>{
        var videos : MutableList<Video> = ArrayList()
        for(channel in vids){
            videos.addAll(channel.videos!!)
        }
        return videos
    }
    fun  getVideos(){
        // 1. seperate videos by category named
     //   progressBar.visibility = View.VISIBLE

        var api = APIClient().getRetrofitInstance()!!.create(DiscoverApi::class.java)
        var discover =  DiscoverRepository(api)

        discover.getChannels()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object: DisposableObserver<MutableList<Channel>>() {

                override fun onNext(channels: MutableList<Channel>) {

                    var videos = getVideosFromChannels(channels)
                    customSuggestionsAdapter.suggestions = videos
                    searchBar.setCustomSuggestionAdapter(customSuggestionsAdapter)
                       // updateContent(latestVideos)
                }

                override fun onComplete() {

                }

                override fun onError(e: Throwable) {
                    // Toast.makeText(requireActivity(),e.message, Toast.LENGTH_LONG).show()

                }


            })

    }


    override fun onSuggestionsVideoClick(view: View, pos: Int, video: Video) {
      //  Toast.makeText(requireContext(),""+video.title, Toast.LENGTH_LONG).show()

        val intent  =  Intent(context, VideoDetailsActivity::class.java)

        intent.putExtra("videoTitle",video.title)
        intent.putExtra("videoDesc",video.description)
        intent.putExtra("videoPrice",video.price)
        var sub = ""+video.pgRating +" | "+video.category+" | "+video.year+" | "+video.runtime
        intent.putExtra("videoDetailsSubtitle", sub)
        intent.putExtra("videoContentId", video.contentId)
        intent.putExtra("videoUrl", video.videoUrl)
        intent.putExtra("videoPosterUrl", video.posterUrl)

        // intent.putExtras(bundle)
        startActivity(intent)


    }



}

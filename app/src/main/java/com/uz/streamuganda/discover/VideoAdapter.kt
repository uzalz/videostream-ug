package com.uz.streamuganda.discover

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.uz.streamuganda.R
import com.uzalz.service_facade.entities.Video
import java.util.ArrayList


class VideoAdapter(var context: Context, private var history: MutableList<Video>, private var contactslistner:Videolistner) : RecyclerView.Adapter<VideoAdapter.VideoHolder>() {




    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoHolder{

        val  view = LayoutInflater.from(parent.context).inflate(R.layout.item_video, parent, false)
        return VideoHolder(view)
    }

    var isBind :Boolean = false

    //2. Our Adapter needs 2 ViewHolders
    class VideoHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var titleText: TextView = itemView.findViewById(R.id.item_video_title)
        private var subtitleText: TextView = itemView.findViewById(R.id.item_video_price)
        private var videoPoster: ImageView = itemView.findViewById(R.id.video_poster)



        fun bind(video: Video, contactslistner: Videolistner) {


            val price = ""+video.price
            subtitleText.text = price
            //for the moment placeholder image


            var videoName = video.title

            if(videoName!!.length > 10){
                var newTitle =  videoName.substring(0,9)
                titleText.text = newTitle
            }else{
                titleText.text = videoName
            }


           // val imageUri = "https://i.imgur.com/tGbaZCY.jpg"
            val imageUri = video.posterUrl
            Picasso.with(itemView.context).load(imageUri)
                .fit().centerCrop()
                .placeholder(R.drawable.ic_movie)
                //.error(R.drawable.user_placeholder_error)
                .into(videoPoster)


            val position = adapterPosition

            itemView.setOnClickListener {
                contactslistner.onVideoClick(itemView,position,video)

            }

        }

    }




    // // Passes the message object to a ViewHolder so that the contents can be bound to UI.
    override fun onBindViewHolder(holder: VideoHolder, position: Int) {
        val video: Video = history[position]
        holder.bind(video,contactslistner)

    }

    override fun getItemCount(): Int {
        return history.size
    }



    fun getVideo(pos: Int): Video {
        return history[pos]
    }





    //Clicklistner implementation
    interface Videolistner{

        fun onVideoClick(view: View, pos:Int, video: Video)

    }






    private lateinit var filteredSuggestions : MutableList<Video>
    fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence): FilterResults {
                val results = FilterResults()
                val term = constraint.toString()


                if (term.isEmpty()) {
                    filteredSuggestions = history
                } else {
                    var filteredList = ArrayList<Video>()
                    for (row in history) { // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.title?.toLowerCase()!!.contains(term.toLowerCase())) {
                            filteredList.add(row)
                            Log.d("CustoomSuggestions", "search found" + term.toLowerCase())
                        }
                    }
                    filteredSuggestions = filteredList
                }

                results.values = filteredSuggestions
                return results
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults) {

                history = results.values as MutableList<Video>
                notifyDataSetChanged()
            }
        }
    }




}
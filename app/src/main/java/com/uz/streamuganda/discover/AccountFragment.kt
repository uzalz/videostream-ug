package com.uz.streamuganda.discover

/**
 * A simple [Fragment] subclass.
 */

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.avatarfirst.avatargenlib.AvatarConstants
import com.firebase.ui.auth.AuthUI
import com.google.android.exoplayer2.video.VideoListener
import com.google.android.material.button.MaterialButton
import com.google.android.material.card.MaterialCardView
import com.google.firebase.auth.FirebaseAuth
import com.makeramen.roundedimageview.RoundedTransformationBuilder
import com.mancj.materialsearchbar.MaterialSearchBar
import com.squareup.picasso.Picasso
import com.squareup.picasso.Transformation
import com.uz.streamuganda.R
import com.uz.streamuganda.studio.ManageChannelActivity
import com.uz.streamuganda.studio.MyChannelActivity
import com.uz.streamuganda.users.SignInActivity
import com.uz.streamuganda.utilities.AppExecutorsUI
import com.uz.streamuganda.utilities.AvatarGenerator
import com.uz.streamuganda.utilities.PreferenceHandler
import com.uzalz.service_facade.APIClient
import com.uzalz.service_facade.discover.DiscoverApi
import com.uzalz.service_facade.discover.DiscoverRepository
import com.uzalz.service_facade.entities.Channel
import com.uzalz.service_facade.entities.Video
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_account.view.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [MovieFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class AccountFragment : Fragment(),CustomSuggestionsAdapter.SuggestionsVideolistner {


    companion object {
        fun newInstance() = AccountFragment()
    }

    //Integrates the main sign up activities and opens up the other activities
    private lateinit var auth: FirebaseAuth


    private lateinit var profileView : ImageView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Here notify the fragment that it should participate in options menu handling.
        setHasOptionsMenu(true)
    }

    lateinit var recycler : RecyclerView
    lateinit var buttonChannel : MaterialCardView

     var userphoto:String ? = null
     lateinit var usernameTv : TextView
     lateinit var useremailTv : TextView;

    private lateinit var buttonSignout : MaterialButton


    private lateinit var searchBar: MaterialSearchBar

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_account, container, false)

        searchBar = v.findViewById<MaterialSearchBar>(R.id.searchBarAccount)


        profileView = v.account_profile_pic


        usernameTv = v.tv_account_username
        useremailTv = v.tv_account_useremail

        buttonSignout = v.findViewById(R.id.account_sign_up)


        buttonChannel = v.button_account_mychannel
        buttonChannel.setOnClickListener {

            var id =  PreferenceHandler(requireContext()).getPref(PreferenceHandler.KEY_CHANNEL_ID)

            if(id!=null){
                loadMyChannel()

            }else{
                val intent = Intent(context,ManageChannelActivity::class.java)
                startActivity(intent)
            }




        }

        setUpToolBar()
        signout()


        setUpSearchBar()
        getSearchVideos()

      //  recycler = v.findViewById(R.id.video_list)

        //setUpRecyclerView()
        // getChannels()

        return v

    }




    /////////////////////searchbar /////////////////////////////////////////////////
    private lateinit var suggestions : ArrayList<Video>
    private lateinit var customSuggestionsAdapter : CustomSuggestionsAdapter

    private fun setUpSearchBar(){
        // Search bar
        //  var searchBar = searchBarDiscover
        //enable searchbar callbacks
        //searchBar.setOnSearchActionListener(this);
        //restore last queries from disk
        //lastSearches = searchBar.getLastSuggestions();
        //searchBar.setLastSuggestions(lastSearches);
        //Inflate menu and setup OnMenuItemClickListener
        searchBar.inflateMenu(R.menu.main);
        //searchBar.setSearchIcon(R.drawable.ic_search);
        //searchBar.setRoundedSearchBarEnabled(true)
        //searchBar.toolbar_main.title = "TubeJet"
        //searchBar.setBackgroundColor(resources.getColor(R.color.windowBackground));
        searchBar.setCardViewElevation(20);
        //searchBar.setDividerColor(R.color.colorAccent);
        //searchBar.setTextColor(R.color.colorWhite)
        //searchBar.setPlaceHolderColor(getResources().getColor(R.color.light_blue));
        searchBar.setNavButtonEnabled(false);
        // SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        // var cn = ComponentName(this, SearchCaretakerActivity.class);
        // searchBar.enableSearch()
        searchBar.showSuggestionsList()

        searchBar.setOnSearchActionListener(object: MaterialSearchBar.OnSearchActionListener {
            override fun onButtonClicked(buttonCode: Int) {

                when (buttonCode) {
                    MaterialSearchBar.BUTTON_BACK -> searchCard.visibility = View.GONE
                    MaterialSearchBar.BUTTON_SPEECH -> searchCard.visibility = View.GONE
                }
            }

            override fun onSearchConfirmed(text: CharSequence?) {

            }

            override fun onSearchStateChanged(enabled: Boolean) {

            }


        })



        searchBar.addTextChangeListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                Log.d("LOG_MAIN_ACTIVITY",  " text changed " + searchBar.getText())
                // searchAdapter.getFilter().filter(searchBar.text);
                customSuggestionsAdapter.filter.filter(searchBar.text);
                // mylist.visibility=View.VISIBLE
                // searchCard.visibility = View.VISIBLE
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (TextUtils.isEmpty(s)){
                    //Text is cleared, do your thing
                    //  searchCard.visibility = View.GONE
                    Log.d("LOG_MAIN_ACTIVITY", " text changed empty now ")
                }

            }

        })

        setUpSearchBarAdapter()

    }

    private fun setUpSearchBarAdapter(){

        val inflater = requireActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        //searchAdapter = CustomSuggestionsAdapter(inflater)
        customSuggestionsAdapter = CustomSuggestionsAdapter(inflater,this)
        ///  suggestions = getSomeVideos()


    }
    fun getVideosFromChannels(vids: MutableList<Channel>):MutableList<Video>{
        var videos : MutableList<Video> = ArrayList()
        for(channel in vids){
            videos.addAll(channel.videos!!)
        }
        return videos
    }
    fun  getSearchVideos(){
        // 1. seperate videos by category named
        //   progressBar.visibility = View.VISIBLE

        var api = APIClient().getRetrofitInstance()!!.create(DiscoverApi::class.java)
        var discover =  DiscoverRepository(api)

        discover.getChannels()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object: DisposableObserver<MutableList<Channel>>() {

                override fun onNext(channels: MutableList<Channel>) {

                    var videos = getVideosFromChannels(channels)
                    customSuggestionsAdapter.suggestions = videos
                    searchBar.setCustomSuggestionAdapter(customSuggestionsAdapter)
                    // updateContent(latestVideos)
                }

                override fun onComplete() {

                }

                override fun onError(e: Throwable) {
                    // Toast.makeText(requireActivity(),e.message, Toast.LENGTH_LONG).show()

                }


            })

    }

    override fun onSuggestionsVideoClick(view: View, pos: Int, video: Video) {
       // Toast.makeText(requireContext(),""+video.title,Toast.LENGTH_LONG).show()
        val intent  =  Intent(context, VideoDetailsActivity::class.java)

        intent.putExtra("videoTitle",video.title)
        intent.putExtra("videoDesc",video.description)
        intent.putExtra("videoPrice",video.price)
        var sub = ""+video.pgRating +" | "+video.category+" | "+video.year+" | "+video.runtime
        intent.putExtra("videoDetailsSubtitle", sub)
        intent.putExtra("videoContentId", video.contentId)
        intent.putExtra("videoUrl", video.videoUrl)
        intent.putExtra("videoPosterUrl", video.posterUrl)

        // intent.putExtras(bundle)
        startActivity(intent)


    }


    ////////////////////////////////////////////////////////////////////////////////





    private fun loadMyChannel(){


        var cId = PreferenceHandler(context!!).getPref(PreferenceHandler.KEY_CHANNEL_ID)
        var cName = PreferenceHandler(context!!).getPref(PreferenceHandler.KEY_CHANNEL_NAME)
        var cDesc = PreferenceHandler(context!!).getPref(PreferenceHandler.KEY_CHANNEL_DESC)

        val intent = Intent(context, MyChannelActivity::class.java)
        intent.putExtra("channelId",cId)
        intent.putExtra("channelName",cName)
        intent.putExtra("channelDesc",cDesc)
        startActivity(intent)



    }

    private fun setUpToolBar() {
        /*
        //reportsFragmentBinding.headerGraph.tvtitleEvents.setTypeface(MyFonts.getBoldTypeFace(getContext()));
       //reportsFragmentBinding.headerGraph.tvSubtitleEvents.setTypeface(MyFonts.getTypeFace(getContext()));
        //((AppCompatActivity)getActivity()).getSupportActionBar().setLogo(R.drawable.notice_48);


        //    toolbar_main.logo = resources.getDrawable(R.drawable.ic_logo)
        // toolbar_main.navigationIcon = resources.getDrawable(R.drawable.ic_arrow_back_black_24dp)

        //supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        //  supportActionBar!!.setIcon(R.drawable.ic_logo)
      //  supportActionBar!!.setDisplayShowTitleEnabled(true)


        var toolbar = view?.findViewById<Toolbar>(R.id.toolbar_account)

        (activity as AppCompatActivity?)?.setSupportActionBar(toolbar)
        val actionbar = (activity as AppCompatActivity?)?.supportActionBar

        val str = SpannableStringBuilder("Account")
        str.setSpan(StyleSpan(Typeface.BOLD), 0, 6, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        toolbar?.title = str

        actionbar?.title = str

         */

    }


    override fun onPrepareOptionsMenu(menu: Menu) {
       // val itemSearch = menu.findItem(R.id.action_studio)
       // itemSearch.isVisible = true
        super.onPrepareOptionsMenu(menu);
        // itemSearch.setVisible(false);
        //  item.setVisible(false);
    }



   override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {

          // First clear current all the menu items
         //  menu.clear()
          //add items
           inflater.inflate(R.menu.menu_account, menu)

       super.onCreateOptionsMenu(menu, inflater)

        /*
        val item = menu.findItem(R.id.action_studio)
        item.isVisible = false
         */
    }



   override fun onOptionsItemSelected(item: MenuItem): Boolean { // Handle action bar item clicks here. The action bar will
// automatically handle clicks on the Home/Up button, so long
// as you specify a parent activity in AndroidManifest.xml.
       when (item.itemId) {
           R.id.action_studio -> {

             //  Toast.makeText(context, " Studio", Toast.LENGTH_LONG).show()

            //   val intent = Intent(context,ManageChannelActivity::class.java)
            //   startActivity(intent)


              var id =  PreferenceHandler(requireContext()).getPref(PreferenceHandler.KEY_CHANNEL_ID)

                   if(id!=null){
                       loadMyChannel()

                   }else{
                       val intent = Intent(context,ManageChannelActivity::class.java)
                       startActivity(intent)
                   }





           }


       }

        return super.onOptionsItemSelected(item)
    }


    /*
     0772427452
    0701119896
     */


    override fun onResume() {
        super.onResume()

        var pref = PreferenceHandler.getInstance(requireContext())
        var username = pref?.getPref(PreferenceHandler.KEY_PROFILE_NAME)
        var useremail = pref?.getPref(PreferenceHandler.KEY_PROFILE_EMAIL)
        userphoto = pref?.getPref(PreferenceHandler.KEY_PROFILE_PHOTO_URL)


      //  var splitUserName = username!!.split(" ")
       // var secondPartName = splitUserName[1].substring(0,1)
       //   var newUserName = splitUserName[0] +" "+secondPartName

        var splitUserName = ""
        if(username==null){
           var splitEmail = useremail!!.split("@")
           splitUserName = splitEmail[0]
        }else{
            splitUserName= username
        }

        usernameTv.text = splitUserName
        useremailTv.text = useremail


        val transformation: Transformation = RoundedTransformationBuilder()
            .borderColor(Color.GRAY)
            .borderWidthDp(3f)
            .cornerRadiusDp(30f)
            .oval(true)
            .build()




        Picasso.with(context).load(userphoto)
            .fit().centerCrop()
            .placeholder(AvatarGenerator.avatarImage(context!!, 100, AvatarConstants.CIRCLE, splitUserName))
            .transform(transformation)
            //.error(R.drawable.user_placeholder_error)
            .into(profileView)

    }




    private fun signout(){



       // FirebaseAuth.getInstance().signOut()

        buttonSignout.setOnClickListener {

            AuthUI.getInstance()
                .signOut(requireContext())
                .addOnCompleteListener {
                    // user is now signed out
                    startActivity(Intent(requireContext(), SignInActivity::class.java))
                    activity!!.finishAffinity()
                }

        }

    }




}


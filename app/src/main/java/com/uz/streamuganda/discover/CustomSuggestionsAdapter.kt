package com.uz.streamuganda.discover

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mancj.materialsearchbar.adapter.SuggestionsAdapter
import com.uz.streamuganda.R
import com.uzalz.service_facade.entities.Video
import java.util.*


class CustomSuggestionsAdapter(inflater: LayoutInflater?, private var listner: SuggestionsVideolistner) : SuggestionsAdapter<Video, CustomSuggestionsAdapter.SuggestionHolder>(inflater) {
    override fun getSingleViewHeight(): Int {
        return 125
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SuggestionHolder {
        val view: View = layoutInflater.inflate(R.layout.item_suggestions, parent, false)
        return SuggestionHolder(view)
    }

    override fun onBindSuggestionHolder(suggestion: Video, holder: SuggestionHolder, position: Int) {
       // holder.title.text = suggestion.title
       // holder.subtitle.text = suggestion.price

        holder.bind(suggestion,listner)
    }

    /**
     * **Override to customize functionality**
     *
     * Returns a filter that can be used to constrain data with a filtering
     * pattern.
     *
     *
     *
     * This method is usually implemented by [androidx.recyclerview.widget.RecyclerView.Adapter]
     * classes.
     *
     * @return a filter used to constrain data
     */
    private lateinit var filteredSuggestions : MutableList<Video>
    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence): FilterResults {
                val results = FilterResults()
                val term = constraint.toString()


                if (term.isEmpty()) {
                    filteredSuggestions = suggestions
                } else {
                    var filteredList = ArrayList<Video>()
                    for (row in suggestions) { // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.title?.toLowerCase()!!.contains(term.toLowerCase())) {
                            filteredList.add(row)
                            Log.d("CustoomSuggestions", "search found" + term.toLowerCase())
                        }
                    }
                    filteredSuggestions = filteredList
                }

                results.values = filteredSuggestions
                return results
            }

               override fun publishResults(constraint: CharSequence?, results: FilterResults) {

                suggestions = results.values as MutableList<Video>
                notifyDataSetChanged()
            }
        }
    }

    class SuggestionHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var title: TextView
        var subtitle: TextView
        protected var image: ImageView? = null

        init {
            title = itemView.findViewById(R.id.item_title)
            subtitle = itemView.findViewById(R.id.item_subtitle)

        }

        fun bind(video: Video, listner: SuggestionsVideolistner) {

            title.text = video.title
            subtitle.text = video.price

            itemView.setOnClickListener {
                listner.onSuggestionsVideoClick(itemView,position,video)
            }
        }

    }

    interface SuggestionsVideolistner{

        fun onSuggestionsVideoClick(view: View, pos:Int, video: Video)

    }

}
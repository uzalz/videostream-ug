package com.uz.streamuganda.studio

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Typeface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.StyleSpan
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.uz.streamuganda.R
import com.uzalz.service_facade.APIClient
import com.uzalz.service_facade.UseCase
import com.uzalz.service_facade.studio.PublishVideo
import com.uzalz.service_facade.studio.StudioApi
import com.uzalz.service_facade.studio.StudioRepository
import com.uz.streamuganda.utilities.DateConverter
import com.uz.streamuganda.utilities.URIPathHelper
import kotlinx.android.synthetic.main.activity_publish.*
import kotlinx.android.synthetic.main.activity_publish.video_view
import kotlinx.android.synthetic.main.activity_publish_content.*
import kotlinx.android.synthetic.main.activity_video_details.*
import java.io.IOException
import java.util.*

class PublishActivity : AppCompatActivity() {



     // instance for firebase storage and StorageReference
    lateinit var  storage : FirebaseStorage

   private var channelId : String ? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_publish)

        setUpToolBar("Publish Video")

        var b = intent.extras
        channelId =  b?.getString("channelId")


        // get the Firebase  storage reference
        storage = FirebaseStorage.getInstance()
        storageReference = storage.reference

        //initialiseExoPlayer()

        goNext()
        requestPermission()


        openGallery("VID")
        setUpVideoUploadControls(channelId!!)



    }


    private  var exoPlayer : SimpleExoPlayer? = null
    private  var playerView : PlayerView? = null

    private var  playWhenReady = true
    private var currentWindow = 0
    private var playbackPosition : Long = 0



    /////////////////////// EXO PLAYER SETUP ///////////////////////////////////////


    private fun initialiseExoPlayer(videoPath: String){

        /*3.Our player can hog a lot of resources including memory, CPU, network connections and hardware codecs.
         Many of these resources are in short supply, particularly for hardware codecs where there may only be one.
          It's important that we release those resources for other apps to use when we're not using them, such as
           when our app is put into the background. Put another way, our player's lifecycle should be tied to the
            lifecycle of our app. To implement this, you'll need to override the four methods onStart, onResume,
            onPause and onStop of PlayerActivity.
         */
        /*
    Starting with API level 24 Android supports multiple windows. As our app can be visible but not active in split window mode,
    we need to initialize the player in onStart. Before API level 24 we wait as long as possible until we grab resources,
     so we wait until onResume before initializing the player.
     */

        playerView = video_view

        if(exoPlayer == null){
            exoPlayer = SimpleExoPlayer.Builder(this).build()
        }

        //set player on player view
        playerView?.player = exoPlayer

        //2. Create a media source - your player now needs some content to play create a media item
        // A progressive media source is one which is obtained through progressive download.

        // val mp3file = " https://storage.googleapis.com/exoplayer-test-media-0/play.mp3"

        var media = getString(R.string.media_url_mp4)
        var media2 = "https://storage.googleapis.com/muxdemofiles/mux-video-intro.mp4"

       // var videoUrl= String.valueOf(Uri.fromFile(myFile));


        val uri = Uri.parse(videoPath)
        val mediaSource = buildMediaSource(uri)

        //Start playing as soon all resources for playback have been acquired, since playwhenready is nitially true
        // playback will start automatically the first time the app is run
        exoPlayer?.playWhenReady = playWhenReady

        //tells the player to seek to a certain position within a specific window. Both currentWindow and playbackPosition
        //  were initialized to zero so that playback starts from the very start the first time the app is run.
        exoPlayer?.seekTo(currentWindow, playbackPosition)

        // tells the player to acquire all the resources for the given mediaSource, and additionally tells it not to
        // reset the position or state, since we already set these in the two previous lines.
        exoPlayer?.prepare(mediaSource!!, false, false)


    }


    private fun buildMediaSource(uri: Uri): MediaSource? {
        // a factory of creating progressive mdeia data sources
        val dataSourceFactory: DataSource.Factory = DefaultDataSourceFactory(this, "exoplayer-local")
        return ProgressiveMediaSource.Factory(dataSourceFactory)
            .createMediaSource(uri)
        /*
        Multimedia data is usually stored using a container format, such as MP4 or Ogg. Before the video and/or audio
         data can be played it must be extracted from the container. ExoPlayer is capable of
        extracting data from many different container formats using a variety of Extractor classes.
         */
    }


    // END OF EXO PLAYER SETUP /////////////////////////////////////////////////////






    private fun goNext(){

            button_publish_next.setOnClickListener {
                if(videoFullPath!=null) {
                    layout_publish_content_extra1.visibility = View.GONE
                    layout_publish_content_extra2.visibility = View.VISIBLE
                    label_publish_heading.text = resources.getString(R.string.label_video_step_2)
                }else{

                    Toast.makeText(this,"Video not selected",Toast.LENGTH_LONG).show()
                }

            }
    }




    private fun openGallery(value : String) {

        //val intent = Intent()
        //intent.type = "video/*"
        //intent.action = Intent.ACTION_GET_CONTENT
       // startActivityForResult(Intent.createChooser(intent, "Select Video"),REQUEST_CODE)
        if(value == "IMG"){
            Toast.makeText(this,"IMG",Toast.LENGTH_SHORT).show()
            if (Build.VERSION.SDK_INT < 20) {
               // var photoPickerIntent = Intent(Intent.ACTION_PICK);
                // photoPickerIntent.type = "video/*"
                // photoPickerIntent.setType("image/* video/*");
                //val  mimetypes = arrayOf("image/*", "video/*")
                //photoPickerIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
                //startActivityForResult(photoPickerIntent, REQUEST_CODE)
            } else {
                var photoPickerIntent = Intent(Intent.ACTION_GET_CONTENT)
                photoPickerIntent.type = "image/*";
                startActivityForResult(photoPickerIntent, REQUEST_CODE_IMG)
            }

          }else if(value == "VID"){

          //   var galleryIntent = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
          //  galleryIntent.type = "video/*"
          //  startActivityForResult(galleryIntent, REQUEST_CODE_VID)



            if (Build.VERSION.SDK_INT < 20) {
               // var photoPickerIntent = Intent(Intent.ACTION_PICK);
                // photoPickerIntent.type = "video/*"
                // photoPickerIntent.setType("image/* video/*");
               // val  mimetypes = arrayOf("image/*", "video/*")
               // photoPickerIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
                //startActivityForResult(photoPickerIntent, REQUEST_CODE)
            } else {
                  var photoPickerIntent = Intent(Intent.ACTION_GET_CONTENT)
                  photoPickerIntent.type = "video/*";
                  startActivityForResult(photoPickerIntent, REQUEST_CODE_VID)
            }


        }



    }


    private var MY_PERMISSIONS_REQUEST_READ_CONTACTS = 25

    private var videoFullPath : String ? = null
    private lateinit var posterImageFullPath : Uri

    private fun requestPermission() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), MY_PERMISSIONS_REQUEST_READ_CONTACTS)

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
        }
    }


    fun isPermissionGranted(permission:String):Boolean =
        ContextCompat.checkSelfPermission(
            this,
            permission
        ) == PackageManager.PERMISSION_GRANTED

    override fun onRequestPermissionsResult(requestCode: Int, thePermissions: Array<String>, theGrantResults: IntArray
    ) {
        // It's not our expect permission
        if (requestCode != MY_PERMISSIONS_REQUEST_READ_CONTACTS) return


        var per = Manifest.permission.READ_EXTERNAL_STORAGE

        if (isPermissionGranted(per)) {
            // Success case: Get the permission
            Toast.makeText(this,"Permission Granted",Toast.LENGTH_SHORT).show()
            // Do something and return
            return
        }

      //  if (isUserCheckNeverAskAgain()) {
            // NeverAskAgain case - Never Ask Again has been checked
            // Do something and return
            return
       // }

        // Failure case: Not getting permission
        // Do something here
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_VID) {

            if (data?.data != null) {
               // var path = data.data!!.path

                    val uriPathHelper = URIPathHelper()
                    videoFullPath = uriPathHelper.getPath(this, data.data) // Use this video path according to your logic
                    Log.d("PublishActivityV", "Video"+videoFullPath)

                    if(videoFullPath!=null) {
                        initialiseExoPlayer(videoFullPath!!)
                    }else{
                        Toast.makeText(this, "Video not received",Toast.LENGTH_LONG).show()
                        videoFullPath = "path/to/video"
                    }
               }
                 //  var d = data.dataString
                 //  videoFullPath = "path/to/video"
            }else if(resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_IMG){

            if (data?.data != null) {

                posterImageFullPath = data.data!!
                Log.d("PublishActivityI", "Image: "+posterImageFullPath);

                if(posterImageFullPath!=null) {
                }else{
                    Toast.makeText(this, "Image not received",Toast.LENGTH_LONG).show()
                }

                try {
                    var bitmap =
                        MediaStore.Images.Media.getBitmap(contentResolver, posterImageFullPath)
                    publish_content_thumbnail.setImageBitmap(bitmap)
                } catch (e: IOException) {
                    e.printStackTrace();
                }
              }

            }
        }


    fun playVideoInDevicePlayer(videoPath: String) {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(videoPath))
        intent.setDataAndType(Uri.parse(videoPath), "video/mp4")
        startActivity(intent)

        //URIPathHelper class is used to get a full Video path from the URI object.
    }


    private fun setUpToolBar(channelName: String) {

        //    toolbar_main.logo = resources.getDrawable(R.drawable.ic_logo)
        // toolbar_main.navigationIcon = resources.getDrawable(R.drawable.ic_arrow_back_black_24dp)
        setSupportActionBar(toolbar_publish)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        //  supportActionBar!!.setIcon(R.drawable.ic_logo)
        supportActionBar!!.setDisplayShowTitleEnabled(true)

        val channelNameSize = channelName.length

        val str = SpannableStringBuilder(channelName)
        str.setSpan(StyleSpan(Typeface.BOLD), 0, channelNameSize, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        toolbar_publish.title = str

       // supportActionBar!!.title = str
        supportActionBar!!.title  = ""


        //toolbar.setTitle("VimsApp");
        //toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimary));
        //((AppCompatActivity)getActivity()).getSupportActionBar().setLogo(R.drawable.notice_48);
        //  val collapsingToolbarLayout: CollapsingToolbarLayout = findViewById(R.id.collapsing_toolbar)

        ///collapsingToolbarLayout.setExpandedTitleGravity(Gravity.BOTTOM);
        ///collapsingToolbarLayout.setExpandedTitleGravity(Gravity.BOTTOM);

        // collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar)
        // collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar)
        //  collapsingToolbarLayout.setCollapsedTitleGravity(Gravity.START)
        //  collapsingToolbarLayout.setTitleEnabled(false)
        //   collapsingToolbarLayout.setTitle("VimsApp")

    }


    var category = ""
    var priceMode = ""


    private fun setUpVideoUploadControls(channelId: String){


        posterUpload.setOnClickListener{

            openGallery("IMG")
        }


        spinnerCategory.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                val item = parent.selectedItem.toString()
                category = item
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        spinnerPrice.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                val item = parent.selectedItem.toString()
                priceMode = item

                if(priceMode == "Free") {
                    videoPrice = "Free"
                    textInputLayoutPublishPrice.visibility = View.INVISIBLE


                }else{

                        textInputLayoutPublishPrice.visibility = View.VISIBLE

                }
            }



            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }



        button_publish.setOnClickListener{

            selectDetails()
          //  publishVideo(channelId,videoViewModel)
            publishThumbNail(channelId)

        }

    }



     lateinit var videoTitle : String
     lateinit var videoDesc : String
     var videoPrice  = "FREE"
     lateinit var releaseDate : Date
    private fun selectDetails(){

         videoTitle =  editInputVideoTitle.text.toString()
         videoDesc =   editInputVideoDesc.text.toString()

        if(editInputVideoPrice.text!!.isNotEmpty() && priceMode == "Paid") {
            videoPrice = editInputVideoPrice.text.toString()
        }
        releaseDate = DateConverter.stringToDateWithoutTime(DateConverter.dateToStringWithoutTime(Date()))!!

    }




    // UploadImage method
    private fun publishThumbNail(channelId:String)
    {
        //publish assets first then get their url and save the data

        // Code for showing progressDialog while uploading
        var progressDialog : ProgressDialog = ProgressDialog(this)
        progressDialog.setTitle("Uploading...")
        progressDialog.show()

        // Defining the child of storageReference
        var ref: StorageReference  = storageReference.child("images/"+ UUID.randomUUID().toString())


       // Upload from a local file
        // adding listeners on upload
        // or failure of image
       var uploadTask =  ref.putFile(posterImageFullPath)
        // Register observers to listen for when the download is done or if it fails
        uploadTask.addOnFailureListener {
            // Handle unsuccessful uploads

            // Error, Image not uploaded
            progressDialog.dismiss();
            Toast.makeText(this, "Failed " + it.message, Toast.LENGTH_SHORT).show();

        }.addOnSuccessListener { taskSnapshot ->
            // taskSnapshot.metadata contains file metadata such as size, content-type, etc.

            // Image uploaded successfully
            // Dismiss dialog
            progressDialog.dismiss();
            Toast.makeText(this, "Image Uploaded!!"+taskSnapshot.uploadSessionUri, Toast.LENGTH_SHORT).show()

            ref.downloadUrl.addOnSuccessListener { downloadUri ->

                Log.d("PublishActivityD","DOWNLOAD URI: "+downloadUri.toString())

                val videoViewModel = PublishActivityViewModel(
                    channelId,
                    videoTitle,
                    videoDesc,
                    "",
                    "",
                    releaseDate,
                    "",
                    category,
                    videoPrice,
                    2020,
                    "PG-13",
                    videoFullPath!!,
                    downloadUri.toString()
                )
                publishVideo(channelId,videoViewModel)

            }.addOnFailureListener {
                // Handle any errors
            }



        }.addOnProgressListener {taskSnapshot ->
              var  progress = (100.0 * taskSnapshot.bytesTransferred / taskSnapshot.totalByteCount)
                            progressDialog.setMessage("Uploaded $progress%")
        }







      }




fun publishVideo(channelId: String,video: PublishActivityViewModel){
    Log.d("PublishActivityD","PUBLISH VIDEO CALLED")

      //  progressBarPublishVideo.visibility = View.VISIBLE

        // Initialise
        var api = APIClient().getRetrofitInstance()!!.create(StudioApi::class.java)
        var studio =  StudioRepository(api)
        var publishVideo = PublishVideo(studio)

        //Make Request
        var request = PublishVideo.RequestValue(channelId,video.title,video.description,video.director,
             video.crew,video.releaseDate,video.language,video.category,video.price,video.year,
             video.pgRating,video.videoPath,video.posterPath)

        //Execute Request
        publishVideo.execute(request, object : UseCase.OutputBoundary<PublishVideo.ResponseValue>{

            override fun onSuccess(result: PublishVideo.ResponseValue) {

                var success = "Video "+result.video?.title+" Has been published"
                Toast.makeText(applicationContext,success,Toast.LENGTH_LONG).show()

               // progressBarPublishVideo.visibility = View.INVISIBLE
                layout_publish_content.visibility = View.INVISIBLE

                layout_publish_content_success.visibility = View.VISIBLE
                button_publish_video_finish.setOnClickListener{

                   finish()

                }

            }

            override fun onInvalidMessage(e: String) {
               // Toast.makeText(applicationContext,success,Toast.LENGTH_LONG).show()
            }

            override fun onNetworkFail(e: String) {

              //  Toast
                Log.d("PublishActivityD", "Error publish"+e)

            }

        })


    }



    ///////////////////////////////////////////////////////////////////////////////////



    override fun onStart() {
        super.onStart()

        if(Util.SDK_INT > 23){

               //  initialiseExoPlayer()
        }


    }


    /*
    Before API Level 24 there is no guarantee of onStop being called. So we have to release the player as early
    as possible in onPause. Starting with API Level 24 (which brought multi and split window mode) onStop is
    guaranteed to be called. In the paused state our activity is still visible so we wait to release the player until onStop.

     We now need to create a releasePlayer method which frees up the player's resources and destroys it.

     */
    override fun onStop() {
        super.onStop()
        if (Util.SDK_INT > 23) {
               releaseExoPlayer()
        }
    }


    override fun onPause() {
        super.onPause()
        //dacastPlayer.onPause()

        if (Util.SDK_INT <= 23) {
             releaseExoPlayer()
        }
    }

    override fun onResume() {
        super.onResume()
        //dacastPlayer.onResume()

        hideSystemUi()

        if(Util.SDK_INT <= 23 || exoPlayer == null){

             //initialiseExoPlayer()
        }
    }

    @SuppressLint("InlinedApi")
    private fun hideSystemUi() {
        playerView?.systemUiVisibility = (View.SYSTEM_UI_FLAG_LOW_PROFILE
                or View.SYSTEM_UI_FLAG_FULLSCREEN
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION)
    }



    private fun releaseExoPlayer() {
        if (exoPlayer != null) {
            playWhenReady = exoPlayer?.playWhenReady!!
            playbackPosition = exoPlayer?.currentPosition!!
            currentWindow = exoPlayer?.currentWindowIndex!!
            exoPlayer?.release()
            exoPlayer = null;
        }
    }







    companion object{

         private const val REQUEST_CODE_IMG = 1
         private const val REQUEST_CODE_VID = 2
         lateinit var storageReference : StorageReference


    }




}

package com.uz.streamuganda.studio


import java.util.*


data class PublishActivityViewModel(val channelId:String, val title:String, val description:String, val director:String, val crew:String,
                            val releaseDate: Date, val language:String, val category:String,
                            val price:String, val year: Int, val pgRating:String,
                            val videoPath:String, val posterPath:String)

package com.uz.streamuganda.studio

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.uz.streamuganda.R


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
private const val ARG_PARAM_DESC = "paramDesc"


/**
 * A simple [Fragment] subclass.
 * Use the [ChannelAboutFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ChannelAboutFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var paramDesc: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
            paramDesc = it.getString(ARG_PARAM_DESC)

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
       var root = inflater.inflate(R.layout.fragment_channel_about, container, false)

          var title =  root.findViewById<TextView>(R.id.tv_about_channel_title)
          var subtitle =  root.findViewById<TextView>(R.id.tv_about_channel_desc)

          title.text  = param2
          subtitle.text = paramDesc


        return root
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ChannelAboutFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String,paramDesc: String) =
            ChannelAboutFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                    putString(ARG_PARAM_DESC, paramDesc)

                }
            }
    }
}

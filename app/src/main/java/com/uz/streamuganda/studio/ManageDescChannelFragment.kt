package com.uz.streamuganda.studio

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.uz.streamuganda.R
import com.uzalz.service_facade.APIClient
import com.uzalz.service_facade.studio.StudioApi
import com.uzalz.service_facade.studio.StudioRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_upload_channel_details.*

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class ManageDescChannelFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_upload_channel_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        //2. create channel online
        var api = APIClient().getRetrofitInstance()!!.create(StudioApi::class.java)
        var studio =  StudioRepository(api)

        view.findViewById<Button>(R.id.button_save_channel_details).setOnClickListener {
            //1. get title
            var channelDesc = editInputChannelDesc.text.toString()
           studio.editChannel("12345", channelDesc)
        }
    }







    fun editChannel(channelId: String,channelDesc: String){
        //showProgress(true)
        var api = APIClient().getRetrofitInstance()!!.create(StudioApi::class.java)
        var studio =  StudioRepository(api)


        studio.editChannel("12345",channelDesc)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object: DisposableObserver<Boolean>() {

                override fun onNext(t: Boolean) {

                    // showProgress(false)

                    view?.findViewById<Button>(R.id.button_create_channel)?.setOnClickListener {
                        findNavController().navigate(R.id.action_CreateChannelFragment_to_CompleteChannelFragment)
                    }

                }

                override fun onComplete() {

                }

                override fun onError(e: Throwable) {
                    Toast.makeText(context,e.message, Toast.LENGTH_LONG).show()
                }


            })
    }





}

package com.uz.streamuganda.studio

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.LinearLayout
import android.widget.ProgressBar
import androidx.annotation.MainThread
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.uz.streamuganda.R
import com.uzalz.service_facade.APIClient
import com.uzalz.service_facade.entities.Channel
import com.uzalz.service_facade.entities.Video
import com.uzalz.service_facade.studio.StudioApi
import com.uzalz.service_facade.studio.StudioRepository

import com.uz.streamuganda.discover.VideoAdapter
import com.uz.streamuganda.utilities.AppExecutorsUI
import com.uz.streamuganda.utilities.PreferenceHandler
import com.uz.streamuganda.utilities.RecyclerManager
import com.uzalz.service_facade.discover.DiscoverApi
import com.uzalz.service_facade.discover.DiscoverRepository
import com.uzalz.service_facade.entities.Subscription
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_channel_details.*
import kotlinx.android.synthetic.main.activity_channel_details.view.*
import kotlinx.android.synthetic.main.activity_my_channel.*
import kotlinx.android.synthetic.main.fragment_channel_home.*
import kotlinx.android.synthetic.main.fragment_channel_home.view.*
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ChannelHomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ChannelHomeFragment : Fragment(),VideoAdapter.Videolistner {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)

        }
    }


    private lateinit var emptyLayout: LinearLayout
    private lateinit var recyclerView: RecyclerView
    private lateinit var progressBar: ProgressBar
    private lateinit var buttonAddVideo : MaterialButton
    private lateinit var fab: FloatingActionButton

    private lateinit var buttonChannelDetails : MaterialButton

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_channel_home, container, false)
        //  var tv =  view?.findViewById<TextView>(R.id.home_channel_title)
        //  tv?.text  = param1

        recyclerView = root.channel_list
        progressBar = root.progressBarLoadChannelVideos
        buttonAddVideo = root.button_video_upload
        emptyLayout = root.layout_empty_channel_videos
        fab = root.fabUploadVideo
        buttonChannelDetails = root.btn_channel_details_subscribe_to


        setUpRecyclerView()

        getChannel()
        setUploadVideo()

       // fab.setOnClickListener{


        //}

        return root
    }

    override fun onResume() {
        super.onResume()
        loadChannel()
    }



    private fun loadChannel(){
     var run = Runnable {

         getChannel()
     }
     AppExecutorsUI().mainThread().execute(run)

    }


    private fun setUploadVideo(){

        buttonAddVideo.setOnClickListener{

            val intent = Intent(context,PublishActivity::class.java)
            intent.putExtra("channelId",param1)
            intent.putExtra("channelName",param2)
            startActivity(intent)

        }

    }

    private fun setUpRecyclerView(){

        // val layoutManager = LinearLayoutManager(context)
        var gridColumns = 3
        val layoutManager =  GridLayoutManager(context, gridColumns, GridLayoutManager.VERTICAL, false)

        recyclerView.layoutManager = layoutManager

        // decoration = DividerItemDecoration(getContext(), VERTICAL)
        // val itemDecoration = ItemOffsetDecoration(getContext(), R.dimen.item_offset_schedule)

        //setting animation on scrolling
        recyclerView.setHasFixedSize(true); //enhance recycler view scroll
        recyclerView.isNestedScrollingEnabled = false;// enable smooth scrooll

        RecyclerManager.decorateRecyclerView(recyclerView,requireContext())
    }

    private fun updateContent(videoList: MutableList<Video>){

        val adapter = VideoAdapter(context!!,videoList, this)
        adapter.setHasStableIds(true)
        adapter.notifyItemInserted(0)
        recyclerView.adapter  = adapter

    }




    fun  getChannel(){

        //Log.d("ChannelHome"," start id: "+param1)

        var api = APIClient().getRetrofitInstance()!!.create(StudioApi::class.java)
        var studio =  StudioRepository(api)

        studio.getChannel(param1!!)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object: DisposableObserver<Channel>() {

            override fun onNext(t: Channel) {

                Log.d("ChannelHome"," name: "+t.channelName+" sizevideos: "+t.videos!!.size)
                progressBar.visibility = View.INVISIBLE

                updateChannelDetails(t)

                if(t.videos!!.isNotEmpty()){

                    emptyLayout.visibility = View.INVISIBLE
                    recyclerView.visibility = View.VISIBLE
                    updateContent(t.videos!!)

                }else{

                    emptyLayout.visibility = View.VISIBLE
                    recyclerView.visibility = View.INVISIBLE

                }

            }

            override fun onComplete() {

            }

            override fun onError(e: Throwable) {
                progressBar.visibility = View.INVISIBLE
                emptyLayout.visibility = View.VISIBLE
                recyclerView.visibility = View.INVISIBLE
            }


        })
    }

    private fun updateChannelDetails(channel: Channel){

              var run = Runnable {

              }
           //   AppExecutorsUI().mainThread().execute(run)


        if(channel.channelName!=null) {
            tv_channel_details_name_.text = channel.channelName  //channell details must not be null
        }
        var sub = ""+channel.getNoSubscriptions()+" subscriptions"
        tv_channel_details_subs_.text = sub



        var api = APIClient().getRetrofitInstance()!!.create(DiscoverApi::class.java)
         var discover =  DiscoverRepository(api)



        btn_channel_details_subscribe_to.setOnClickListener{

            var id =  PreferenceHandler.getInstance(requireContext())!!.getPref(PreferenceHandler.KEY_PROFILE_ID)

            var subscription = Subscription()
            subscription.userId = id!!
            subscription.dateOfSubscription = Date()
            subscription.subscriptionId = UUID.randomUUID().toString()

            discover.subscribeToChannel(channel.channelId!!,subscription)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object: DisposableObserver<Subscription>() {

                    override fun onComplete() {

                    }

                    override fun onError(e: Throwable) {

                    }

                    override fun onNext(t: Subscription) {

                        buttonChannelDetails.text = "SUBSCRIBED"
                       // btn_channel_details_subscribe.icon = resources.getDrawable(R.drawable.ic_thumb_up_black_24dp)
                        buttonChannelDetails.isEnabled = false
                    }

                })


        }


    }


    override fun onVideoClick(view: View, pos: Int, video: Video) {

    }













    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ChannelHomeFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ChannelHomeFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)

                }
            }
    }
}

package com.uz.streamuganda.studio

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.uz.streamuganda.R
import com.uzalz.service_facade.APIClient
import com.uzalz.service_facade.entities.Channel
import com.uzalz.service_facade.studio.StudioApi
import com.uzalz.service_facade.studio.StudioRepository
import com.uz.streamuganda.utilities.PreferenceHandler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_upload_channel.*

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class ManageCreateChannelFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var layout =  inflater.inflate(R.layout.fragment_upload_channel, container, false)

        return layout
    }

    var isChannelCreated = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var editTv = view.findViewById<EditText>(R.id.editInputChannelTitle);
        var buttonCreate = view.findViewById<Button>(R.id.button_create_channel)

        buttonCreate?.setOnClickListener {
                showProgress(true)

                //1. get title
                var channelTitle = editTv?.text.toString()
                //2. create channel online
                val channel = Channel()
                channel.channelName = channelTitle
                channel.channelDescription = ""
                channel.channelAvatorUrl = ""

                //   Toast.makeText(context,channelTitle+" "+channel.channelName,Toast.LENGTH_LONG).show()

                createChannel(channel)

        }


    }


    fun createChannel(channel: Channel){
        showProgress(true)
        var api = APIClient().getRetrofitInstance()!!.create(StudioApi::class.java)
        var studio =  StudioRepository(api)

        //Toast.makeText(context,channel.channelName,Toast.LENGTH_LONG).show()

        studio.createChannel(channel)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object: DisposableObserver<Channel>() {

            override fun onNext(t: Channel) {

                showProgress(false)

                layout_channel_create_success.visibility = View.VISIBLE
                layout_channel_create.visibility = View.INVISIBLE

                tv_channel_name_success.text = t.channelName
                button_create_channel_finish.setOnClickListener{

                    //   bundle.putString("channel",gson.toString())
                    val intent = Intent(context,MyChannelActivity::class.java)
                    intent.putExtra("channelId",t.channelId)
                    intent.putExtra("channelName",t.channelName)
                    intent.putExtra("channelDesc",t.channelDescription)
                    startActivity(intent)

                    PreferenceHandler(context!!).putPref(PreferenceHandler.KEY_CHANNEL_ID,t.channelId)
                    PreferenceHandler(context!!).putPref(PreferenceHandler.KEY_CHANNEL_NAME,t.channelName)
                    PreferenceHandler(context!!).putPref(PreferenceHandler.KEY_CHANNEL_DESC,t.channelDescription)



                }

                Log.d("ManageCreateChannel","Name: "+t.channelName)
                /*
                view?.findViewById<Button>(R.id.button_create_channel)?.setOnClickListener {
                    findNavController().navigate(R.id.action_CreateChannelFragment_to_CompleteChannelFragment)
                }
                 */

            }

            override fun onComplete() {

            }

            override fun onError(e: Throwable) {
                Toast.makeText(context,e.message,Toast.LENGTH_LONG).show()
                Log.d("ManageCreateChannel",""+e.localizedMessage)
            }


        })
    }



    fun showProgress(progress: Boolean){

        if(progress){
            progressBarCreateChannel.visibility = View.VISIBLE
        }
        else{
            progressBarCreateChannel.visibility = View.INVISIBLE
        }

    }


}

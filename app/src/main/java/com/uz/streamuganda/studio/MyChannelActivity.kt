package com.uz.streamuganda.studio

import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.StyleSpan
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.viewpager.widget.ViewPager
import com.google.gson.Gson
import com.google.gson.stream.JsonReader
import com.uz.streamuganda.R
import com.uzalz.service_facade.entities.Channel
import com.uz.streamuganda.discover.ViewPagerAdapter
import kotlinx.android.synthetic.main.activity_my_channel.*
import java.io.StringReader


class MyChannelActivity : AppCompatActivity(){

    var channelName = "Default"
    var channelDesc = ""
    var channelSub = ""
    var channelId = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_channel)

        val bundle = intent.extras


        if(bundle!=null){
            channelId = bundle.getString("channelId").toString()
            channelName = bundle.getString("channelName").toString()
            channelDesc = bundle.getString("channelDesc").toString()
            channelSub = bundle.getString("channelSubs").toString()


            Log.d("MyChannelActivityLog","ChannelDesc: "+channelDesc)

        }else{
            Log.d("MyChannelActivityLog","ChannelName Empty")
        }

         /*
           val channel = intent.getParcelableExtra<Channel>("channel")
           Log.d("MyChannelActivityLog","ChannelName: "+channel?.channelName)
           val channel = intent.getParcelableExtra<Channel>("channel")
         */


      //  channel?.channelName?.let { setupViewPager(it) }
        setUpToolBar(channelName)
        setupViewPager(channelId,channelName,channelDesc)

        tv_my_channel_name.text = channelName
        tv_my_channel_subs.text = channelSub




    }




    fun fromGsonStringToChannel(value: String): Channel {
      //  val listType = object : TypeToken<Channel>() {}.type

       // return Gson().fromJson(value, listType)

        val reader = JsonReader(StringReader(value))
        reader.isLenient = true

        val mychannel: Channel = Gson().fromJson(reader, Channel::class.java)

        return mychannel
    }



    private lateinit var  toolbar : Toolbar
    private fun setUpToolBar(channelName: String) {

        toolbar = findViewById<Toolbar>(R.id.toolbar_channel)
        toolbar.setNavigationOnClickListener{
            finish()
        }

        //    toolbar_main.logo = resources.getDrawable(R.drawable.ic_logo)
        // toolbar_main.navigationIcon = resources.getDrawable(R.drawable.ic_arrow_back_black_24dp)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        //  supportActionBar!!.setIcon(R.drawable.ic_logo)
        supportActionBar!!.setDisplayShowTitleEnabled(true)

        val channelNameSize = channelName.length

        val str = SpannableStringBuilder(channelName)
        str.setSpan(StyleSpan(Typeface.BOLD), 0, channelNameSize, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        toolbar.title = str

        supportActionBar!!.title = str




        //toolbar.setTitle("VimsApp");
        //toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimary));
        //((AppCompatActivity)getActivity()).getSupportActionBar().setLogo(R.drawable.notice_48);
      //  val collapsingToolbarLayout: CollapsingToolbarLayout = findViewById(R.id.collapsing_toolbar)

        ///collapsingToolbarLayout.setExpandedTitleGravity(Gravity.BOTTOM);
        ///collapsingToolbarLayout.setExpandedTitleGravity(Gravity.BOTTOM);

        // collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar)
        // collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar)
      //  collapsingToolbarLayout.setCollapsedTitleGravity(Gravity.START)
      //  collapsingToolbarLayout.setTitleEnabled(false)
     //   collapsingToolbarLayout.setTitle("VimsApp")

    }



    private fun setupViewPager(channelId: String, channelName: String, channelDesc: String) { //setup the viewpager--- it contains the fragments to be swiped
        val viewPager: ViewPager = viewpager_channel
        //assign viewpager to tablayout
        tabs_channel.setupWithViewPager(viewPager)

        val adapter = ViewPagerAdapter(supportFragmentManager) //child
        adapter.addFragment(ChannelHomeFragment.newInstance(channelId,channelName), "Home")
        adapter.addFragment(ChannelAboutFragment.newInstance(channelId,channelName,channelDesc), "About")
        viewPager.adapter = adapter
    }




    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.menu_channel_upload, menu)
        return true



    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {

            R.id.action_menu_upload->{

                val intent = Intent(this,PublishActivity::class.java)
                intent.putExtra("channelId",channelId)
                intent.putExtra("channelName",channelName)
                startActivity(intent)
                return true
            }  // Do Activity menu item stuff here

            else -> {
            }

        }
        return false
    }



}

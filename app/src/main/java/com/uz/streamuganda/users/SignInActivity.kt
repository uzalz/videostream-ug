package com.uz.streamuganda.users

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider
import com.uz.streamuganda.MainActivity
import com.uz.streamuganda.R
import com.uz.streamuganda.utilities.MyFonts
import com.uz.streamuganda.utilities.PreferenceHandler

import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login.progressBar


class SignInActivity : AppCompatActivity() {


    //Integrates the main sign up activities and opens up the other activities
    private lateinit var auth: FirebaseAuth


    private lateinit var googleSignInClient: GoogleSignInClient


    private lateinit var userText: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fullscreen()
        setContentView(R.layout.activity_login)

        tV.typeface = MyFonts.getTypeFace(this)

        auth = FirebaseAuth.getInstance()
        Log.d("WelcomeIN", "CREATE: ")
       //  val intent = Intent(this, ChooserActivity::class.java)
       //  val intent = Intent(this, FirebaseUIActivity::class.java)
       //  startActivity(intent)
        if (auth.currentUser != null) { // already signed in
            Log.d("WelcomeIN", "SIGN C: "+auth.currentUser?.email!!)
            //Go to main screen
            loadWelcomeView()

        } else { // not signed in
            //Prepare google to signin
            requestGoogleToSignIn()
            signInBtn()
            signUpBtn()
            enterEmailAndPassword()

          //  Log.d("WelcomeIN", "SIGN C: "+auth.currentUser?.email!!)

        }




    }


    override fun onStart() {
        super.onStart()

        // Check if user is signed in (non-null) and update UI accordingly.
      //  signOut()
        val currentUser = auth.currentUser

        if (auth.currentUser != null) { // already signed in
            //Go to main screen
            loadWelcomeView()

        } else { // not signed in
            //Prepare google to signin
            requestGoogleToSignIn()
            signInBtn()
            signUpBtn()

        }

    }


    private fun fullscreen(){

        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
    }

    private fun requestGoogleToSignIn(){

        // [START config_signin]
        // Configure Google Sign In
        // We tell google to open the email account to allow user login using your email
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()  //show emails of user
            .requestProfile()
            .build()
        // [END config_signin]

        googleSignInClient = GoogleSignIn.getClient(this, gso)

    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)

           // val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)

            // The Task returned from this call is always completed, no need to attr a listener.
            // The Task returned from this call is always completed, no need to attach

            try {
                // Google Sign In was successful, authenticate with Firebase
                   val account = task.getResult(ApiException::class.java)!!

                    val acct = GoogleSignIn.getLastSignedInAccount(this)
                    //val account = result?.signInAccount
                    Log.d(TAG, "firebaseAuthWithGoogle:" + account.id)

                        firebaseAuthWithGoogle(account.idToken!!)



                    ///////if signed in show user details from last signed in account

                    //val userInfo = acct?.email
                    // updateNewUI(userInfo!!)
                    //Toast.makeText(this, userInfo, Toast.LENGTH_LONG).show()




            } catch (e: ApiException) {
                // Google Sign In failed, update UI appropriately
                Log.d("SignInActivity-E", "Google sign in failed", e)
                // [START_EXCLUDE]
                //    updateUI(null)
                // [END_EXCLUDE]
                Toast.makeText(this," G Authentication Failed."+{e.localizedMessage}, Toast.LENGTH_LONG).show()
            }


        }
    }
    // [END onactivityresult]



    // [START auth_with_google]
    private fun firebaseAuthWithGoogle(idToken: String) {
        // [START_EXCLUDE silent]
        showLoginUI(false)
        // [END_EXCLUDE]
        val credential = GoogleAuthProvider.getCredential(idToken, null)
        auth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInWithCredential:success")
                    val user = auth.currentUser
                      updateUI(user!!)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "signInWithCredential:failure", task.exception)
                    // [START_EXCLUDE]
                    // val view = binding.mainLayout
                    // [END_EXCLUDE]
                    // Snackbar.make(view, "Authentication Failed.", Snackbar.LENGTH_SHORT).show()

                    Toast.makeText(this,"Authentication Failed.",Toast.LENGTH_LONG).show()
                   // updateUI(null)
                }

                // [START_EXCLUDE]
                hideProgressBar()
                // [END_EXCLUDE]
            }
    }
    // [END auth_with_google]



    fun enterEmailAndPassword(){


        buttonSignIn.setOnClickListener {

            var email =  emailInputLogin.editableText.toString()
            var username = passInputLogin.editableText.toString()

            signInWithEmailAndPassword(email,username)

        }



    }


    fun signInWithEmailAndPassword(email:String, password:String){

        showProgressBar()
        showLoginUI(false)

        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInWithEmail:success")
                    val user = auth.currentUser
                    updateUI(user!!)
                } else {
                    showLoginUI(true)
                    hideProgressBar()
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "signInWithEmail:failure", task.exception)
                    Toast.makeText(baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()
                    //updateUI(null)
                }
            }
    }


    fun updateUI(user : FirebaseUser){

        var pref = PreferenceHandler.getInstance(this)
        pref?.putPref(PreferenceHandler.KEY_PROFILE_ID, user.uid)
        pref?.putPref(PreferenceHandler.KEY_PROFILE_NAME, user.displayName)
        pref?.putPref(PreferenceHandler.KEY_PROFILE_EMAIL, user.email)
        if(user.photoUrl !=null) {
            pref?.putPref(PreferenceHandler.KEY_PROFILE_PHOTO_URL, user.photoUrl.toString())
        }

        loadWelcomeView()

    }



    private fun showLoginUI(isShowLogin: Boolean){
       if(isShowLogin){

           imageID.visibility = View.VISIBLE
           tV.visibility = View.VISIBLE
           loginlayout.visibility = View.VISIBLE
           account.visibility = View.VISIBLE

       }else{
           imageID.visibility = View.GONE
           tV.visibility = View.GONE
           loginlayout.visibility = View.GONE
           account.visibility = View.GONE

       }

    }

    private fun signInBtn() {

       buttonSignInGoogle.setOnClickListener{
            val signInIntent = googleSignInClient.signInIntent
            startActivityForResult(signInIntent, RC_SIGN_IN)

        }

    }

    private fun signUpBtn() {

        buttonSignUp.setOnClickListener{
            val signInIntent = Intent(this, SignupActivity::class.java)
            startActivity(signInIntent)

        }

    }


    // [END sign in]
    private fun signOut() {
        // Firebase sign out
        auth.signOut()

        // Google sign out
        googleSignInClient.signOut().addOnCompleteListener(this) {
           // updateUI(null)
        }


    }

    private fun revokeAccess() {
        // Firebase sign out
        //Disconnect from firebase
        auth.signOut()

        // Google revoke access
        googleSignInClient.revokeAccess().addOnCompleteListener(this) {
            //update UI to null values
        }
    }



    private fun hideProgressBar(){
        progressBar.visibility = View.INVISIBLE
    }
    private fun showProgressBar(){
        progressBar.visibility = View.VISIBLE
    }



    companion object {
        private const val TAG = "MainActivity"
        private const val RC_SIGN_IN = 9001
    }



    private fun loadWelcomeView() {
          hideProgressBar()
          finish()
          val intent = Intent(this, MainActivity::class.java)
          intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
          intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
          startActivity(intent)
    }



    private fun openHomeView(){

        /*
        var frag  = HomeFragment.newInstance()

        val hfragmentManager: FragmentManager = supportFragmentManager
        hfragmentManager.beginTransaction()
            .replace(R.id.signup_fragment, frag)
            .commit()

         */

    }



}

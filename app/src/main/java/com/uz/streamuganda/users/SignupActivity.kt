package com.uz.streamuganda.users

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.uz.streamuganda.MainActivity
import com.uz.streamuganda.R
import com.uz.streamuganda.utilities.PreferenceHandler
import kotlinx.android.synthetic.main.activity_signup.*

class SignupActivity : AppCompatActivity() {



    private lateinit var auth: FirebaseAuth


    private lateinit var signUpButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)

        // Initialize Firebase Auth
        auth = Firebase.auth

        signUpButton = findViewById(R.id.buttonSignUpNow)
        emailInput = findViewById(R.id.emailInput)
        usernameInput = findViewById(R.id.usernameInput)


        if(auth.currentUser != null){

        }

        signUp()

    }

    private lateinit var emailInput:TextInputEditText
    private lateinit var usernameInput:TextInputEditText

    private fun signUp(){

        signUpButton.setOnClickListener {

            var email =  emailInput.editableText.toString()
            var username = usernameInput.editableText.toString()

            createUser(email,username)

        }


    }




    private fun createUser(email:String,pass:String){
        progressBarSignUp.visibility = View.VISIBLE
        showSignUpUI(false)

        auth.createUserWithEmailAndPassword(email, pass)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    //  Log.d(TAG, "createUserWithEmail:success")

                    progressBarSignUp.visibility = View.INVISIBLE
                    loadHomeView()

                   // Toast.makeText(this,"createUserWithEmail:success",Toast.LENGTH_LONG).show()
                    val user = auth.currentUser

                    var pref =PreferenceHandler.getInstance(this)
                    pref?.putPref(PreferenceHandler.KEY_PROFILE_NAME,user?.displayName)
                    pref?.putPref(PreferenceHandler.KEY_PROFILE_EMAIL,user?.email)
                    pref?.putPref(PreferenceHandler.KEY_PROFILE_PHOTO_URL,user?.photoUrl.toString())


                 //updateUI(user)
                } else {
                    progressBarSignUp.visibility = View.INVISIBLE
                    showSignUpUI(true)
                    // If sign in fails, display a message to the user.
                   //  Log.w(TAG, "createUserWithEmail:failure", task.exception)
                    Toast.makeText(baseContext, "Authentication failed."+task.exception, Toast.LENGTH_SHORT).show()
                   // updateUI(null)
                }


            }



    }

    private fun loadHomeView(){

        val intentHome = Intent(this, MainActivity::class.java)
        startActivity(intentHome)

    }




    private fun showSignUpUI(isShowLogin: Boolean){
        if(isShowLogin){

            imageID.visibility = View.VISIBLE
            textInputLayoutEmail.visibility = View.VISIBLE
            textInputLayoutPassword.visibility = View.VISIBLE
            backToSignIn.visibility = View.VISIBLE
            buttonSignUpNow.visibility = View.VISIBLE

        }else{

            imageID.visibility = View.GONE
            textInputLayoutEmail.visibility = View.GONE
            textInputLayoutPassword.visibility = View.GONE
            backToSignIn.visibility = View.GONE
            buttonSignUpNow.visibility = View.GONE

        }

    }




}
